package android.sportsworld.appsell.sportsworld.Invitados;

import android.content.Context;
import android.content.SharedPreferences;
import android.sportsworld.appsell.sportsworld.R;
import android.sportsworld.appsell.sportsworld.conectionManager.ConnectionManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EnviarInvitacion extends AppCompatActivity {

    private EditText nombre,correo;
    Button enviar;
    SharedPreferences prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enviar_invitacion);

        prefs = this.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        nombre = (EditText) findViewById(R.id.nombre);
        correo = (EditText) findViewById(R.id.email);
        enviar = (Button) findViewById(R.id.enviar);

        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!nombre.toString().isEmpty() && !correo.toString().isEmpty()){
                    JSONObject json = new JSONObject();
                    try{
                        json.put("idPersona",Integer.toString(prefs.getInt("user_id",0)));
                        json.put("to",correo.getText());
                        json.put("nameto",nombre.getText());
                        json.put("subject","Pase de invitado");
                    }catch (Exception e){
                        Log.d("holamundo",e.getLocalizedMessage().toString());
                    }

                    RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),json.toString());
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl("http://sandbox.sportsworld.com.mx")
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    ConnectionManager manager = retrofit.create(ConnectionManager.class);
                    Call<ResponseBody> usuario = manager.sendEmailInvitado(body);
                    usuario.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                            Toast.makeText(getApplicationContext(),response.message().toString(),Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {

                            Toast.makeText(getApplicationContext(),t.getMessage().toString(),Toast.LENGTH_LONG).show();
                        }
                    });

                }else {
                    Toast.makeText(getApplicationContext(), "Algún dato esta vacío", Toast.LENGTH_LONG).show();
                }
            }
        });


    }
}
