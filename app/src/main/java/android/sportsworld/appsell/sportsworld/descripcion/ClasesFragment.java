package android.sportsworld.appsell.sportsworld.descripcion;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.LocationListener;
import android.os.Bundle;
import android.sportsworld.appsell.sportsworld.Modelo.CancelarClase;
import android.sportsworld.appsell.sportsworld.Modelo.Clases.Clases;
import android.sportsworld.appsell.sportsworld.Modelo.Clases.DatumClases;
import android.sportsworld.appsell.sportsworld.Modelo.ClubsDatos.ClubDatos;
import android.sportsworld.appsell.sportsworld.Modelo.ReservacionClase;
import android.sportsworld.appsell.sportsworld.R;
import android.sportsworld.appsell.sportsworld.conectionManager.ConnectionManager;
import android.sportsworld.appsell.sportsworld.tools.SortHour;
import android.sportsworld.appsell.sportsworld.tools.calendario.HorizontalCalendar;
import android.sportsworld.appsell.sportsworld.tools.calendario.utils.HorizontalCalendarListener;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import in.abacritt.android.library.SectionedMergeAdapter;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by victor.lagunas on 17/01/18.
 */

public class ClasesFragment extends Fragment implements AdaptadorClases.ItemClickListener {

    private HorizontalCalendar horizontalCalendar;
    private RecyclerView mRecyclerView;
    private AdaptadorClases mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private SharedPreferences prefs;
    int idClub;
    private String[] myDataset = new String[20];
    public List<DatumClases> datumClases;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_clases,container,false);
        mRecyclerView = rootView.findViewById(R.id.my_recycler_view);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this.getActivity(), DividerItemDecoration.HORIZONTAL);
        mRecyclerView.addItemDecoration(itemDecoration);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), 0));
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);
        prefs = this.getActivity().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        idClub= getArguments().getInt("idss",0);


/* ends after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

        horizontalCalendar = new HorizontalCalendar.Builder(rootView, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(7)
                .configure()
                .formatTopText("MMM")
                .formatMiddleText("dd")
                .formatBottomText("EEE")
                .textSize(14f, 14f, 14f)
                .showTopText(true)
                .showBottomText(true)
                .textColor(Color.LTGRAY, Color.WHITE)
                .end()
                .build();
        horizontalCalendar.getDefaultStyle().setColorBottomText(Color.BLACK);

        horizontalCalendar.getDefaultStyle().setColorCircle(Color.BLACK);
        horizontalCalendar.getSelectedItemStyle().setColorBottomText(Color.WHITE);


        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {

            @Override
            public void onDateSelected(Calendar date, int position) {
                String mesText;
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date.getTime());
                calendar.get(Calendar.DAY_OF_MONTH); //Day of the month :)
                calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                if (month < 10){
                    mesText = "0"+Integer.toString(month+1);
                }
                else{
                    mesText = Integer.toString(month+1);
                }
                int year = calendar.get(Calendar.YEAR);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                final ProgressDialog progressDoalog;
                progressDoalog = new ProgressDialog(getContext());
                progressDoalog.setMax(100);
                progressDoalog.setMessage("");
                progressDoalog.setTitle("Cargando clases");
// show it
                progressDoalog.show();
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("https://prepago.sportsworld.com.mx")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                ConnectionManager servicio =retrofit.create(ConnectionManager.class);

                Call<Clases> datosClub = servicio.getClases(Integer.toString(idClub),Integer.toString(year),mesText,Integer.toString(day),Integer.toString(prefs.getInt("user_id",0)));
                datosClub.enqueue(new Callback<Clases>() {
                    @Override
                    public void onResponse(Call<Clases> call, Response<Clases> response) {
                        Log.d("dia",response.message());

                        datumClases = new ArrayList<DatumClases>();
                        try {
                            if (response.body().getData().size() > 0) {
                                datumClases = response.body().getData();


                                mRecyclerView.setHasFixedSize(true);

                                // use a linear layout manager
                                mLayoutManager = new LinearLayoutManager(getContext());

                                mRecyclerView.setLayoutManager(mLayoutManager);
                                Collections.sort(datumClases,new SortHour());
                                // specify an adapter (see also next example)
                                mAdapter = new AdaptadorClases(datumClases);

                                progressDoalog.dismiss();


                                mAdapter.setClickListener(new AdaptadorClases.ItemClickListener() {
                                    @Override
                                    public void onItemClick(View view, int position) {
                                        if (datumClases.get(position).getInscrito()){
                                            SharedPreferences prefs = getActivity().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
                                            JSONObject json = new JSONObject();

                                            try{
                                                json.put("personaact",Integer.toString(prefs.getInt("user_id",0)));
                                                json.put("fechahora",datumClases.get(position).getIniciovigencia()+datumClases.get(position).getInicio());
                                                json.put("idclub","0");
                                                json.put("idsalon",datumClases.get(position).getIdsalon());
                                                json.put("idclase",datumClases.get(position).getClase());
                                                json.put("usuario","mefistoxxx");
                                                json.put("instalacionactprogramada",datumClases.get(position).getIdinstalacionactividadprogramada());
                                            }catch (Exception e){
                                                Log.d("error",e.getLocalizedMessage().toString());
                                            }
                                            progressDoalog.show();
                                            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),json.toString());
                                            Retrofit retrofit = new Retrofit.Builder()
                                                    .baseUrl("https://app.sportsworld.com.mx")
                                                    .addConverterFactory(GsonConverterFactory.create())
                                                    .build();
                                            ConnectionManager manager = retrofit.create(ConnectionManager.class);
                                            final Call<CancelarClase> cancelar = manager.cancelarReservation(body);
                                            cancelar.enqueue(new Callback<CancelarClase>() {
                                                @Override
                                                public void onResponse(Call<CancelarClase> call, Response<CancelarClase> response) {
                                                    CancelarClase cancelarClaseTemp = response.body();
                                                    Toast.makeText(getContext(),cancelarClaseTemp.getMessage(),Toast.LENGTH_LONG).show();
                                                    progressDoalog.dismiss();
                                                }

                                                @Override
                                                public void onFailure(Call<CancelarClase> call, Throwable t) {
                                                    progressDoalog.dismiss();
                                                }
                                            });
                                        }
                                    }
                                });
                                mRecyclerView.setAdapter(mAdapter);
                            } else {
                                Toast.makeText(getContext(), "Ocurrio un error", Toast.LENGTH_LONG).show();
                                progressDoalog.dismiss();
                            }
                        }catch (Exception e){
                            Toast.makeText(getContext(), "Ocurrio un error", Toast.LENGTH_LONG).show();
                            progressDoalog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<Clases> call, Throwable t) {
progressDoalog.dismiss();
                    }
                });


                //Toast.makeText(getContext(), DateFormat.format("MMM, d, yyyy", date) + " fue seleccionado!", Toast.LENGTH_SHORT).show();
            }

        });


        String mesText;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(Calendar.getInstance().getTime());
        calendar.get(Calendar.DAY_OF_MONTH); //Day of the month :)
        calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        if (month < 10){
            mesText = "0"+Integer.toString(month+1);
        }
        else{
            mesText = Integer.toString(month+1);
        }
        int year = calendar.get(Calendar.YEAR);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(getContext());
        progressDoalog.setMax(100);
        progressDoalog.setMessage("");
        progressDoalog.setTitle("Cargando clases");
// show it
        progressDoalog.show();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://prepago.sportsworld.com.mx")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ConnectionManager servicio =retrofit.create(ConnectionManager.class);

        Call<Clases> datosClub = servicio.getClases(Integer.toString(idClub),Integer.toString(year),mesText,Integer.toString(day),Integer.toString(prefs.getInt("user_id",0)));
        datosClub.enqueue(new Callback<Clases>() {
            @Override
            public void onResponse(Call<Clases> call, Response<Clases> response) {
                Log.d("dia",response.message());

                datumClases = new ArrayList<DatumClases>();
                try {
                    if (response.body().getData().size() > 0) {
                        datumClases = response.body().getData();

                        Log.d("cantidad", String.valueOf(datumClases.size()));

                        // use this setting to improve performance if you know that changes
                        // in content do not change the layout size of the RecyclerView
                        mRecyclerView.setHasFixedSize(true);

                        // use a linear layout manager
                        mLayoutManager = new LinearLayoutManager(getContext());

                        mRecyclerView.setLayoutManager(mLayoutManager);
                        Collections.sort(datumClases,new SortHour());
                        // specify an adapter (see also next example)
                        mAdapter = new AdaptadorClases(datumClases);
                        mAdapter.setClickListener(new AdaptadorClases.ItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                if (datumClases.get(position).getInscrito()){
                                    SharedPreferences prefs = getActivity().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
                                    JSONObject json = new JSONObject();

                                    try{
                                        json.put("personaact",Integer.toString(prefs.getInt("user_id",0)));
                                        json.put("fechahora",datumClases.get(position).getIniciovigencia()+datumClases.get(position).getInicio());
                                        json.put("idclub","0");
                                        json.put("idsalon",datumClases.get(position).getIdsalon());
                                        json.put("idclase",datumClases.get(position).getClase());
                                        json.put("usuario","mefistoxxx");
                                        json.put("instalacionactprogramada",datumClases.get(position).getIdinstalacionactividadprogramada());
                                    }catch (Exception e){
                                        Log.d("error",e.getLocalizedMessage().toString());
                                    }

                                    RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),json.toString());
                                    Retrofit retrofit = new Retrofit.Builder()
                                            .baseUrl("https://app.sportsworld.com.mx")
                                            .addConverterFactory(GsonConverterFactory.create())
                                            .build();
                                    ConnectionManager manager = retrofit.create(ConnectionManager.class);
                                    final Call<CancelarClase> cancelar = manager.cancelarReservation(body);
                                    cancelar.enqueue(new Callback<CancelarClase>() {
                                        @Override
                                        public void onResponse(Call<CancelarClase> call, Response<CancelarClase> response) {
                                            CancelarClase cancelarClaseTemp = response.body();
                                            Toast.makeText(getContext(),cancelarClaseTemp.getMessage(),Toast.LENGTH_LONG).show();
                                        }

                                        @Override
                                        public void onFailure(Call<CancelarClase> call, Throwable t) {

                                        }
                                    });
                                }
                            }
                        });
                        mRecyclerView.setAdapter(mAdapter);
                    } else {
                        Toast.makeText(getContext(), "Ocurrio un error", Toast.LENGTH_LONG).show();
                    }
                }catch (Exception e){
                    Toast.makeText(getContext(), "Ocurrio un error", Toast.LENGTH_LONG).show();
                    Log.d("error",e.getLocalizedMessage());
                }
                progressDoalog.dismiss();
            }

            @Override
            public void onFailure(Call<Clases> call, Throwable t) {
                progressDoalog.dismiss();
            }
        });



        return rootView;
    }

    @Override
    public void onItemClick(View view, int position) {

    }
}
