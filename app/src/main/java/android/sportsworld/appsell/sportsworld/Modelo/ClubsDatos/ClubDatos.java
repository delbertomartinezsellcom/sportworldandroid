package android.sportsworld.appsell.sportsworld.Modelo.ClubsDatos;

import com.google.gson.annotations.SerializedName;

public class ClubDatos {
    public Boolean status;
    public String message;
    @SerializedName("data")
    public DataClubs data;

    public ClubDatos(Boolean status, String message, DataClubs data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataClubs getData() {
        return data;
    }

    public void setData(DataClubs data) {
        this.data = data;
    }
}
