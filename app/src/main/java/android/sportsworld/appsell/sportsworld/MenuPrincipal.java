package android.sportsworld.appsell.sportsworld;

import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub_;
import android.sportsworld.appsell.sportsworld.Modelo.EarnIt;
import android.sportsworld.appsell.sportsworld.Modelo.EarnItData;
import android.sportsworld.appsell.sportsworld.Modelo.Success;
import android.sportsworld.appsell.sportsworld.Noticias.Noticias;
import android.sportsworld.appsell.sportsworld.Perfil.EditPerfil;
import android.sportsworld.appsell.sportsworld.conectionManager.ConnectionManager;
import android.sportsworld.appsell.sportsworld.descripcion.Main3Activity;
import android.sportsworld.appsell.sportsworld.tools.CircleTransform;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MenuPrincipal extends Fragment {

    private TextView mTextMessage;
    private ImageView imagenProfile;
    private SharedPreferences prefs;
    private Button membresiaButton;
    private Button earnit;
    private ImageButton settings;
    private TextView nombre;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.menuprincipalfragment, container, false);

        settings = (ImageButton) v.findViewById(R.id.settings);
        prefs = this.getActivity().getSharedPreferences("Preferences", Context.MODE_PRIVATE);

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent configuraciones = new Intent(getActivity().getApplication(), EditPerfil.class);
                startActivity(configuraciones);
            }
        });


        membresiaButton = (Button) v.findViewById(R.id.membresiaButton);
        earnit = (Button) v.findViewById(R.id.earnit);
        nombre = (TextView) v.findViewById(R.id.nombre);

        nombre.setText(prefs.getString("name",""));
        membresiaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(v.getContext(),MiCredencial.class);
                startActivity(intent);
            }
        });

        earnit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject json = new JSONObject();
                try {
                    json.put("id", prefs.getInt("user_id", 0));
                }catch (Exception e){

                }
                RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),json.toString());
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://manticore.4p.com.mx")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                ConnectionManager manager = retrofit.create(ConnectionManager.class);
                Call<EarnIt> earnit = manager.getEarIt(body);
                earnit.enqueue(new Callback<EarnIt>() {
                    @Override
                    public void onResponse(Call<EarnIt> call, Response<EarnIt> response) {
                        if ( response.isSuccessful()) {
                            EarnIt earnIt = response.body();
                            EarnItData data = earnIt.getData();
                            Toast.makeText(getActivity(), "Puntos: "+Integer.toString(data.getPuntos()), Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(getActivity(), "Ocurrio un error, el servicio no esta disponible", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<EarnIt> call, Throwable t) {
                        Toast.makeText(getActivity(),t.getMessage().toString(),Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        imagenProfile = (ImageView) v.findViewById(R.id.imagenPhotoPerfil);

        Log.d("profile_phoot",prefs.getString("profile_photo",""));

        if (prefs.getString("profile_photo_last","").isEmpty()){
            Handler uiHandler = new Handler(Looper.getMainLooper());
            uiHandler.post(new Runnable() {
                @Override
                public void run() {
                    String path = prefs.getString("profile_photo","");
                    Picasso.get().load(path).priority(Picasso.Priority.HIGH).fit().centerCrop().transform(new CircleTransform()).into(imagenProfile);
                }
            });
        }else{
            Handler uiHandler = new Handler(Looper.getMainLooper());
            uiHandler.post(new Runnable() {
                @Override
                public void run() {
                    String path = prefs.getString("profile_photo_last","");
                    Picasso.get().load(path).priority(Picasso.Priority.HIGH).fit().centerCrop().transform(new CircleTransform()).into(imagenProfile);
                }
            });
        }

        mTextMessage = (TextView) v.findViewById(R.id.message);


        return v;

    }

    @Override
    public void onStart() {
        super.onStart();
        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                String path = prefs.getString("profile_photo","");
                Picasso.get().load(path).priority(Picasso.Priority.HIGH).fit().centerCrop().transform(new CircleTransform()).into(imagenProfile);
            }
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                String path = prefs.getString("profile_photo","");
                Picasso.get().load(path).priority(Picasso.Priority.HIGH).fit().centerCrop().transform(new CircleTransform()).into(imagenProfile);
            }
        });

    }
}

