package android.sportsworld.appsell.sportsworld.Modelo.ClubsDatos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataClubs {
    public Double latitud;
    public int agendar;
    public List<Instalacione> instalaciones;
    public Boolean favorito;
    public String direccion;
    public String horario;
    public int idun;
    @SerializedName("imagenes_club")
    public List<String> imagenesClub;
    public String ruta360;
    public String nombre;
    public String rutavideo;
    public int preventa;
    public String telefono;
    public Double longitud;

    public DataClubs(Double latitud, int agendar, List<Instalacione> instalaciones, Boolean favorito, String direccion, String horario, int idun, List<String> imagenesClub, String ruta360, String nombre, String rutavideo, int preventa, String telefono, Double longitud) {
        this.latitud = latitud;
        this.agendar = agendar;
        this.instalaciones = instalaciones;
        this.favorito = favorito;
        this.direccion = direccion;
        this.horario = horario;
        this.idun = idun;
        this.imagenesClub = imagenesClub;
        this.ruta360 = ruta360;
        this.nombre = nombre;
        this.rutavideo = rutavideo;
        this.preventa = preventa;
        this.telefono = telefono;
        this.longitud = longitud;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public int getAgendar() {
        return agendar;
    }

    public void setAgendar(int agendar) {
        this.agendar = agendar;
    }

    public List<Instalacione> getInstalaciones() {
        return instalaciones;
    }

    public void setInstalaciones(List<Instalacione> instalaciones) {
        this.instalaciones = instalaciones;
    }

    public Boolean getFavorito() {
        return favorito;
    }

    public void setFavorito(Boolean favorito) {
        this.favorito = favorito;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public int getIdun() {
        return idun;
    }

    public void setIdun(int idun) {
        this.idun = idun;
    }

    public List<String> getImagenesClub() {
        return imagenesClub;
    }

    public void setImagenesClub(List<String> imagenesClub) {
        this.imagenesClub = imagenesClub;
    }

    public String getRuta360() {
        return ruta360;
    }

    public void setRuta360(String ruta360) {
        this.ruta360 = ruta360;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRutavideo() {
        return rutavideo;
    }

    public void setRutavideo(String rutavideo) {
        this.rutavideo = rutavideo;
    }

    public int getPreventa() {
        return preventa;
    }

    public void setPreventa(int preventa) {
        this.preventa = preventa;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }
}
