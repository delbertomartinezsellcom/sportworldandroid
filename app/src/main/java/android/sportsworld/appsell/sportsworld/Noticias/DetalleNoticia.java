package android.sportsworld.appsell.sportsworld.Noticias;

import android.sportsworld.appsell.sportsworld.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;

public class DetalleNoticia extends AppCompatActivity {

    WebView webView;
    TextView tituloTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_noticia);
        String titulo = getIntent().getStringExtra("titulo");
        String html = getIntent().getStringExtra("html");

        webView = (WebView)findViewById(R.id.html);

        tituloTextView = (TextView)findViewById(R.id.titulo);
        tituloTextView.setText(titulo);

        webView.loadData(html, "text/html", null);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
    }
}
