package android.sportsworld.appsell.sportsworld.GaleriaInfo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.Club;
import android.sportsworld.appsell.sportsworld.Modelo.ClubsDatos.ClubDatos;
import android.sportsworld.appsell.sportsworld.Modelo.ClubsDatos.DataClubs;
import android.sportsworld.appsell.sportsworld.conectionManager.ConnectionManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.sportsworld.appsell.sportsworld.R;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by victor.lagunas on 18/01/18.
 */

public class InformacionFragmento extends Fragment {
    RecyclerView horizontal_recycler_view;
    HorizontalAdapter horizontalAdapter;
    private List<String> data;
    int idClub;
    TextView textoDireccion;
    TextView textoTelefono;
    public static InformacionFragmento newInstance() {
        InformacionFragmento fragment = new InformacionFragmento();

        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_informacion_fragmento,container,false);

        idClub= getArguments().getInt("idss",0);

        textoDireccion = (TextView)v.findViewById(R.id.textoDireccion);

        textoDireccion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String map = "http://maps.google.co.in/maps?q=" + textoDireccion.getText();
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
                startActivity(i);
            }
        });
        textoTelefono= (TextView) v.findViewById(R.id.textoTelefono);
        textoTelefono.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + textoTelefono.getText()));
                startActivity(intent);
            }
        });
        horizontal_recycler_view= (RecyclerView)v.findViewById(R.id.horizontal_recycler_view);
        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(getContext());
        progressDoalog.setMax(100);
        progressDoalog.setMessage("");
        progressDoalog.setTitle("Cargando información");

// show it
        progressDoalog.show();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://app.sportsworld.com.mx")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ConnectionManager servicio =retrofit.create(ConnectionManager.class);
        Call<ClubDatos> datosClub = servicio.getDatosClub(Integer.toString(idClub));
        datosClub.enqueue(new Callback<ClubDatos>() {
            @Override
            public void onResponse(Call<ClubDatos> call, Response<ClubDatos> response) {
                DataClubs clubDatos = response.body().getData();
                Log.d("adiosmundo",clubDatos.getDireccion());
                data = clubDatos.getImagenesClub();
                textoDireccion.setText(clubDatos.getDireccion());
                textoTelefono.setText(clubDatos.getTelefono());
                horizontalAdapter=new HorizontalAdapter(data, getActivity());

                LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                horizontal_recycler_view.setLayoutManager(horizontalLayoutManager);
                horizontal_recycler_view.setAdapter(horizontalAdapter);
                progressDoalog.dismiss();
            }

            @Override
            public void onFailure(Call<ClubDatos> call, Throwable t) {

                Log.d("adiosmundo",t.getLocalizedMessage().toString());
                data = fill_with_data();
                horizontalAdapter=new HorizontalAdapter(data, getActivity());

                LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                horizontal_recycler_view.setLayoutManager(horizontalLayoutManager);
                horizontal_recycler_view.setAdapter(horizontalAdapter);
                progressDoalog.dismiss();
            }
        });

        return v;
    }

    public List<String> fill_with_data() {

        List<String> data = new ArrayList<>();

        data.add("https://quiosco.sportsworld.com.mx/imagenes/fotosclubes/santa_fe/foto/16_1.jpg");
        data.add("https://quiosco.sportsworld.com.mx/imagenes/fotosclubes/santa_fe/foto/16_1.jpg");
        data.add("https://quiosco.sportsworld.com.mx/imagenes/fotosclubes/santa_fe/foto/16_1.jpg");
        data.add("https://quiosco.sportsworld.com.mx/imagenes/fotosclubes/santa_fe/foto/16_1.jpg");
        data.add("https://quiosco.sportsworld.com.mx/imagenes/fotosclubes/santa_fe/foto/16_1.jpg");
        data.add("https://quiosco.sportsworld.com.mx/imagenes/fotosclubes/santa_fe/foto/16_1.jpg");

        return data;
    }
}
