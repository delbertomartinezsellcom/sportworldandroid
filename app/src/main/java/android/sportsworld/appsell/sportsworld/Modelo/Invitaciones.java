package android.sportsworld.appsell.sportsworld.Modelo;

import java.util.List;

public class Invitaciones {

    public Invitaciones(List<Pases> pases, List<Asignados> asignados) {
        this.pases = pases;
        this.asignados = asignados;
    }

    private List<Pases> pases;
    private List<Asignados> asignados;

    public List<Pases> getPases() {
        return pases;
    }

    public void setPases(List<Pases> pases) {
        this.pases = pases;
    }

    public List<Asignados> getAsignados() {
        return asignados;
    }

    public void setAsignados(List<Asignados> asignados) {
        this.asignados = asignados;
    }
}
