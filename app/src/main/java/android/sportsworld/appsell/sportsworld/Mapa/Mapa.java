package android.sportsworld.appsell.sportsworld.Mapa;

import android.Manifest;
import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.sportsworld.appsell.sportsworld.MainActivity;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.Club;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.Datum;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub_;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub__;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub___;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.P1;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.P2;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.P3;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.P4;
import android.sportsworld.appsell.sportsworld.R;
import android.sportsworld.appsell.sportsworld.conectionManager.ConnectionManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Mapa extends AppCompatActivity implements OnMapReadyCallback, MyRecyclerViewAdapter.ItemClickListener, LocationListener,SearchView.OnQueryTextListener {
    private GoogleMap mMap;
    SupportMapFragment mapFragment;

    SearchView searchview;
    TextView locationTv,p1title,p2title,p3title,p4title;
    LatLng latLng;
    Double home_long, home_lat;
    String addressText;
    MarkerOptions markerOptions;
    CameraPosition cameraZoom;

    private ListClubsAdapter adapter;
    private ListClubsAdapter adapter2;
    private ListClubsAdapter adapter3;
    private ListClubsAdapter adapter4;
    RecyclerView recyclerView;
    RecyclerView recyclerView2;
    RecyclerView recyclerView3;
    RecyclerView recyclerView4;

    private Location location;
    private LocationManager locationManager;

    Geocoder geocoder;
    Address addresses;
    CameraPosition camera;

    public List<Datum> datum;
    public List<ListClub__> listClubs1;
    public List<ListClub> listClub2;
    public List<ListClub_> listClub3;
    public List<ListClub___> listClub4;

    public List<P4> p4;
    public List<P3> p3;
    public List<P2> p2;
    public List<P1> p1;

    public List<P4> personalizado5;

    private Marker marker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);

        locationTv = (TextView) findViewById(R.id.latlongLocation);
        searchview = (SearchView) findViewById(R.id.searchView1);


        recyclerView = (RecyclerView) findViewById(R.id.rvAnimals1);
        recyclerView2 = (RecyclerView) findViewById(R.id.rvAnimals2);
        recyclerView3 = (RecyclerView) findViewById(R.id.rvAnimals3);
        recyclerView4 = (RecyclerView) findViewById(R.id.rvAnimals4);
        p1title =(TextView) findViewById(R.id.p1title);
        p2title =(TextView) findViewById(R.id.p2title);
        p3title =(TextView) findViewById(R.id.p3title);
        p4title =(TextView) findViewById(R.id.p4title);


        searchview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchview.setFocusable(true);
                searchview.setIconified(false);
                searchview.requestFocusFromTouch();
            }
        });

        searchview.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (v == searchview) {
                    if (hasFocus) {
                        // Open keyboard
                        ((InputMethodManager) getBaseContext().getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(searchview, InputMethodManager.SHOW_FORCED);
                    } else {
                        // Close keyboard
                        ((InputMethodManager) getBaseContext().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(searchview.getWindowToken(), 0);
                    }
                }
            }
        });

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

        try {
            int gpsSignal = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
            if (gpsSignal == 0) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } catch (Exception e) {

        }

    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Add a marker in Sydney, Australia,
        // and move the map's camera to the same location.
        mMap = googleMap;
        locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100000, 0,  this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100000, 0,  this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    private void zoomLocation(Location location){
        camera = new CameraPosition.Builder()
                .target(new LatLng(location.getLatitude(),location.getLongitude()))
                .zoom(15)
                .bearing(0)
                .tilt(30)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camera));

    }

    @Override
    public void onItemClick(View view, int position) {
        //Toast.makeText(this, "You clicked " + adapter.getItem(position) + " on item position " + position, Toast.LENGTH_LONG).show();
    }


    @Override
    public void onLocationChanged(Location location) {
        //Toast.makeText(getApplicationContext(),"¡Te estoy buscando!",Toast.LENGTH_LONG).show();
        Double latitude = location.getLatitude();
        Double longitud = location.getLongitude();
        Log.d("latitude",Double.toString(latitude));
        Log.d("longitud",Double.toString(longitud));

        if(markerOptions == null){
            markerOptions = new MarkerOptions();
            markerOptions.position(new LatLng(location.getLatitude(),location.getLongitude()));
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_ubicacion_3x));
            markerOptions.draggable(true);
            mMap.addMarker(markerOptions);
        }else{
            markerOptions.position(new LatLng(location.getLatitude(),location.getLongitude()));
        }

        zoomLocation(location);

        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(Mapa.this);
        progressDoalog.setMax(100);
        progressDoalog.setMessage("");
        progressDoalog.setTitle("Cargando información");
// show it
        progressDoalog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://app.sportsworld.com.mx")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ConnectionManager servicio =retrofit.create(ConnectionManager.class);
        Call<Club> clubs = servicio.getClubs(Double.toString(latitude),Double.toString(longitud));
        clubs.enqueue(new Callback<Club>() {
            @Override
            public void onResponse(Call<Club> call, Response<Club> response) {

                datum = new ArrayList<Datum>();
                datum = response.body().getData();

                p1 = new ArrayList<P1>();
                p2 = new ArrayList<P2>();
                p3 = new ArrayList<P3>();
                p4 = new ArrayList<P4>();
               // personalizado5 = new ArrayList<P4>();


                p1 = datum.get(0).getP1();
                p2  = datum.get(0).getP2();
                p3 = datum.get(0).getP3();
                p4 = datum.get(0).getP4();

                listClubs1 = new ArrayList<ListClub__>();
                listClubs1 = p1.get(0).getListClub();
                List<ListClub> listClub2 = new ArrayList<ListClub>();
                listClub2 = p2.get(0).getListClub();
                List<ListClub_> listClub3 = new ArrayList<ListClub_>();
                listClub3 = p3.get(0).getListClub();
                 List<ListClub_> listClub4 = new ArrayList<ListClub_>();
                listClub4 = p4.get(0).getListClub();

                List<ListClub_> listPersonalizada = new ArrayList<>();
                listPersonalizada=p3.get(0).getListClub();
                for (int i = 0; i<listClub4.size();i++){
                    listPersonalizada.add(listClub4.get(i));

                }


                Log.d("aqui",p4.get(0).getName());
                Log.d("aqui",p3.get(0).getName());
                Log.d("aqui",p2.get(0).getName());
                Log.d("aqui",p1.get(0).getName());
                Log.d("aqui",Integer.toString(listClubs1.size()));

                int height = 80;
                int width = 80;
                BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.sw_pin_3x);
                Bitmap b=bitmapdraw.getBitmap();
                Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
                for (int i =0; i<listClubs1.size();i++){

                    MarkerOptions markerOptions;
                    markerOptions = new MarkerOptions();
                    markerOptions.position(new LatLng(listClubs1.get(i).getLatitud(),listClubs1.get(i).getLongitud()));
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(smallMarker));
                    markerOptions.draggable(true);
                    mMap.addMarker(markerOptions);

                }
                for (int i =0; i<listClub2.size();i++){

                    MarkerOptions markerOptions;
                    markerOptions = new MarkerOptions();
                    markerOptions.position(new LatLng(listClub2.get(i).getLatitud(),listClub2.get(i).getLongitud()));
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(smallMarker));
                    markerOptions.draggable(true);
                    mMap.addMarker(markerOptions);

                }
                for (int i =0; i<listClub3.size();i++){

                    MarkerOptions markerOptions;
                    markerOptions = new MarkerOptions();
                    markerOptions.position(new LatLng(listClub3.get(i).getLatitud(),listClub3.get(i).getLongitud()));
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(smallMarker));
                    markerOptions.draggable(true);
                    mMap.addMarker(markerOptions);

                }
                for (int i =0; i<listClub4.size();i++){

                    MarkerOptions markerOptions;
                    markerOptions = new MarkerOptions();
                    markerOptions.position(new LatLng(listClub4.get(i).getLatitud(),listClub4.get(i).getLongitud()));
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(smallMarker));
                    markerOptions.draggable(true);
                    mMap.addMarker(markerOptions);

                }



                p1title.setText(p1.get(0).getName());
                LinearLayoutManager horizontalLayoutManagaer
                        = new LinearLayoutManager(Mapa.this, LinearLayoutManager.HORIZONTAL, false);
                recyclerView.setLayoutManager(horizontalLayoutManagaer);
                adapter = new ListClubsAdapter(getBaseContext(),p1.get(0).getListClub(),p2.get(0).getListClub(),p3.get(0).getListClub(),p4.get(0).getListClub(),1);

                recyclerView.setAdapter(adapter);



                p2title.setText(p2.get(0).getName());
                LinearLayoutManager horizontalLayoutManagaer2
                        = new LinearLayoutManager(Mapa.this, LinearLayoutManager.HORIZONTAL, false);
                recyclerView2.setLayoutManager(horizontalLayoutManagaer2);
                adapter2 = new ListClubsAdapter(getBaseContext(),p1.get(0).getListClub(),p2.get(0).getListClub(),p3.get(0).getListClub(),p4.get(0).getListClub(),2);
               // adapter2.setClickListener(this);
                recyclerView2.setAdapter(adapter2);

                p3title.setText(p3.get(0).getName());
                LinearLayoutManager horizontalLayoutManagaer3
                        = new LinearLayoutManager(Mapa.this, LinearLayoutManager.HORIZONTAL, false);
                recyclerView3.setLayoutManager(horizontalLayoutManagaer3);
                adapter3 = new ListClubsAdapter(getBaseContext(),p1.get(0).getListClub(),p2.get(0).getListClub(),p3.get(0).getListClub(),p4.get(0).getListClub(),3);
                //adapter2.setClickListener(this);
                recyclerView3.setAdapter(adapter3);

                p4title.setText(p4.get(0).getName());
                LinearLayoutManager horizontalLayoutManagaer4
                        = new LinearLayoutManager(Mapa.this, LinearLayoutManager.HORIZONTAL, false);
                recyclerView4.setLayoutManager(horizontalLayoutManagaer4);
                adapter4 = new ListClubsAdapter(getBaseContext(),p1.get(0).getListClub(),p2.get(0).getListClub(),p3.get(0).getListClub(),p4.get(0).getListClub(),4);
                //adapter2.setClickListener(this);
                recyclerView4.setAdapter(adapter4);
                progressDoalog.dismiss();
            }

            @Override
            public void onFailure(Call<Club> call, Throwable t) {
                Log.d("aqui",t.getLocalizedMessage().toString());
                progressDoalog.dismiss();
            }
        });

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String text = newText;
        adapter.filter(text);
        return false;
    }


}
