package android.sportsworld.appsell.sportsworld.conectionManager;

import android.content.SharedPreferences;
import android.sportsworld.appsell.sportsworld.Modelo.CallBeneficios;
import android.sportsworld.appsell.sportsworld.Modelo.CallNoticias;
import android.sportsworld.appsell.sportsworld.Modelo.CancelarClase;
import android.sportsworld.appsell.sportsworld.Modelo.Clases.Clases;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.Club;
import android.sportsworld.appsell.sportsworld.Modelo.ClubsDatos.ClubDatos;
import android.sportsworld.appsell.sportsworld.Modelo.EarnIt;
import android.sportsworld.appsell.sportsworld.Modelo.Invitaciones;
import android.sportsworld.appsell.sportsworld.Modelo.RegistroRespuesta;
import android.sportsworld.appsell.sportsworld.Modelo.ReservacionClase;
import android.sportsworld.appsell.sportsworld.Modelo.Success;
import android.sportsworld.appsell.sportsworld.Modelo.UpdateData;
import android.sportsworld.appsell.sportsworld.Modelo.Usuario;

import org.json.JSONObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by victor.lagunas on 11/01/18.
 */

public interface ConnectionManager {

//Login
    @POST("api/v2/login_new/")
    Call<Success> usuarioLogin(
            @Header("auth-key") String headerKey
    );
    //Registro
    @Headers({
            "Content-Type: application/json",
            "secret-key: 4614aa2a5e94e308c816e981ffe93f10"
    })
    @POST("/app/api/v1/usuario/registro")
    Call<RegistroRespuesta> setRegister(@Body RequestBody params);

    //Recuperar contraseña
    @Headers({
            "Content-Type: application/json",
            "secret-key: 4614aa2a5e94e308c816e981ffe93f10"
    })
    @POST("app/api/v1/usuario/pass/recuperar")
    Call<ResponseBody> setRecoveryPassword(@Body RequestBody params);

    //Traer invitaciones
    @GET("/app/api/v1/pases/readPasesPorMembresia/{idUser}")
    Call<Invitaciones> getInvitaciones(@Path(value="idUser", encoded = true) String idUser);

    //Enviar Invitación
    @Headers({
            "Content-Type: application/json",
            "secret-key: 4614aa2a5e94e308c816e981ffe93f10"
    })
    @POST("/app/api/v1/pases/mailPaseInvitado")
    Call<ResponseBody> sendEmailInvitado(@Body RequestBody params);

    //Traer Beneficios
    @GET("/beneficios/api/v1/readBeneficio/")
    Call<CallBeneficios> getBeneficios();

    //earn it
    @Headers({
            "Content-Type:application/json",
            "tokenCliente:5p0r75w0rlD09632"
    })
    @POST("/manticore/beneficiarioRewards/puntos")
    Call<EarnIt> getEarIt(@Body RequestBody params);

    //Traer Noticias
    @GET("/api/v2/news")
    Call<CallNoticias> getNoticias();

    //Traer Clubs
    @GET("/api/v2/club_list/{latitude}/{longitude}/274978/")
    Call<Club> getClubs(@Path(value="latitude", encoded = true) String latitud,@Path(value="longitude", encoded = true) String longitude);

    //Trear Datos Club

    @GET("/api/v2/club/details/{idClub}/0")
    Call<ClubDatos> getDatosClub(@Path(value = "idClub",encoded = true) String idClub);

    //Traer Clases
    @GET("/api/v1/app/class/list/reservation/{club}/{ano}/{mes}/{dia}/{idUsuario}")
    Call<Clases> getClases(@Path(value = "club",encoded = true) String club,
                           @Path(value = "ano",encoded = true) String ano,
                           @Path(value = "mes",encoded = true) String mes,
                           @Path(value = "dia",encoded = true) String dia,
                           @Path(value = "idUsuario",encoded = true) String idUsuario);

    //Hacer Reservación
    @Headers({
            "Content-Type: application/json",
            "secret-key: 4614aa2a5e94e308c816e981ffe93f10"
    })
    @POST("/api/v1/app/class/reservation")
    Call<ReservacionClase> doReservation(@Body RequestBody params);

    //Eliminar reservacion
    @Headers({
            "Content-Type: application/json",
            "secret-key: 4614aa2a5e94e308c816e981ffe93f10",
            "Accept:application/json"
    })
    @POST("/api/v2/class/cancelar_reservacion/")
    Call<CancelarClase> cancelarReservation(@Body RequestBody params);

    //Update profile
    @Headers({
            "Content-Type: application/json",
            "secret-key: 4614aa2a5e94e308c816e981ffe93f10",
    })
    @POST("/api/v2/profile/update/v2/")
    Call<UpdateData> updateProfile(@Body RequestBody params);

}



