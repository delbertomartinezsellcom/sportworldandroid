package android.sportsworld.appsell.sportsworld.beneficios;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.sportsworld.appsell.sportsworld.Modelo.Asignados;
import android.sportsworld.appsell.sportsworld.Modelo.BeneficiosObject;
import android.sportsworld.appsell.sportsworld.Modelo.Pases;
import android.sportsworld.appsell.sportsworld.R;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.List;

public class ListBeneficiosAdapter extends BaseAdapter {

    private Context context;
    private int layout;
    private List<BeneficiosObject> beneficios;

    public ListBeneficiosAdapter(Context context, int layout,List<BeneficiosObject> beneficios) {
        this.context = context;
        this.layout = layout;
        this.beneficios = beneficios;
    }

    @Override
    public int getCount() {
        return this.beneficios.size();
    }

    @Override
    public Object getItem(int position) {
        return this.beneficios.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup viewGroup) {
        View v = convertView;
        LayoutInflater layoutInflater = LayoutInflater.from(this.context);
        v = layoutInflater.inflate(R.layout.listbeneficios,null);

        BeneficiosObject beneficio = beneficios.get(position);

        ImageView imagen = (ImageView) v.findViewById(R.id.imagenLogo);
       // ByteArrayOutputStream baos = new ByteArrayOutputStream();
       // byte[] imageBytes = baos.toByteArray();
       // imageBytes = Base64.decode(beneficio.getLogotipo(), Base64.DEFAULT);
       // Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
       // imagen.setImageBitmap(decodedImage);
        Picasso.get().load(beneficio.getLogotipo()).into(imagen);
        TextView text =(TextView) v.findViewById(R.id.textoDescripcion);
        text.setText(beneficio.getNombre());
        return v;
    }
}
