package android.sportsworld.appsell.sportsworld.Seguros;

import android.sportsworld.appsell.sportsworld.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Seguros extends AppCompatActivity {
        TextView texto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seguros);
        texto = (TextView) findViewById(R.id.textoSeguros);
        texto.setMovementMethod(new ScrollingMovementMethod());
    }
}
