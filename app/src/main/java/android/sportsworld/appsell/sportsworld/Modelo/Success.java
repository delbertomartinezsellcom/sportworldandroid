package android.sportsworld.appsell.sportsworld.Modelo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

/**
 * Created by victor.lagunas on 11/01/18.
 */

public class Success {

    boolean status;
    String message;
    Usuario data;

    public Success(boolean status, String message, Usuario data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public Usuario getData() {
        return data;
    }

    public void setData(Usuario data) {
        this.data = data;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
