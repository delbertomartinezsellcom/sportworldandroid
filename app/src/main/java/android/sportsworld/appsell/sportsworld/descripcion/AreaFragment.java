package android.sportsworld.appsell.sportsworld.descripcion;

import android.support.v4.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.sportsworld.appsell.sportsworld.R;
import android.support.annotation.Nullable;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.Calendar;



/**
 * Created by victor.lagunas on 17/01/18.
 */

public class AreaFragment extends Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_area,container,false);
    }
}
