package android.sportsworld.appsell.sportsworld.Modelo;

public class ReservacionClase {
    public Boolean status;
    public String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ReservacionClase(Boolean status, String message) {
        this.status = status;
        this.message = message;
    }
}
