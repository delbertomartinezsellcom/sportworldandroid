package android.sportsworld.appsell.sportsworld.Modelo;

public class BeneficiosObject {
    public int idBeneficio;
    public String nombre;
    public String clausulas;
    public String logotipo;

    public BeneficiosObject(int idBeneficio, String nombre, String clausulas, String logotipo) {
        this.idBeneficio = idBeneficio;
        this.nombre = nombre;
        this.clausulas = clausulas;
        this.logotipo = logotipo;
    }

    public int getIdBeneficio() {
        return idBeneficio;
    }

    public void setIdBeneficio(int idBeneficio) {
        this.idBeneficio = idBeneficio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getClausulas() {
        return clausulas;
    }

    public void setClausulas(String clausulas) {
        this.clausulas = clausulas;
    }

    public String getLogotipo() {
        return logotipo;
    }

    public void setLogotipo(String logotipo) {
        this.logotipo = logotipo;
    }
}
