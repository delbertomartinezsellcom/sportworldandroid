package android.sportsworld.appsell.sportsworld.beneficios;

import android.content.SharedPreferences;
import android.sportsworld.appsell.sportsworld.Invitados.AdaptadorAsignadosList;
import android.sportsworld.appsell.sportsworld.Invitados.DisponiblesFragment;
import android.sportsworld.appsell.sportsworld.Invitados.PageInvitationAdapter;
import android.sportsworld.appsell.sportsworld.Modelo.Asignados;
import android.sportsworld.appsell.sportsworld.Modelo.BeneficiosObject;
import android.sportsworld.appsell.sportsworld.Modelo.CallBeneficios;
import android.sportsworld.appsell.sportsworld.Modelo.Invitaciones;
import android.sportsworld.appsell.sportsworld.R;
import android.sportsworld.appsell.sportsworld.conectionManager.ConnectionManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Beneficios extends AppCompatActivity {

    private ListView listView;
    private List<BeneficiosObject> beneficios;
    View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beneficios);
        listView = (ListView) findViewById(R.id.listaBeneficio);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://sandbox.sportsworld.com.mx")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ConnectionManager servicio =retrofit.create(ConnectionManager.class);
        Call<CallBeneficios> callBeneficios = servicio.getBeneficios();
        callBeneficios.enqueue(new Callback<CallBeneficios>() {
            @Override
            public void onResponse(Call<CallBeneficios> call, Response<CallBeneficios> response) {
                beneficios =new ArrayList<BeneficiosObject>();
                beneficios = response.body().getBeneficios();

                ListBeneficiosAdapter adaptador = new ListBeneficiosAdapter(Beneficios.this,R.layout.listbeneficios,beneficios);
                listView.setAdapter(adaptador);
            }

            @Override
            public void onFailure(Call<CallBeneficios> call, Throwable t) {

            }
        });

    }
}
