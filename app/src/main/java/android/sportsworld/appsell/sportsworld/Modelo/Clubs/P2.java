package android.sportsworld.appsell.sportsworld.Modelo.Clubs;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class P2 {
    public String name;
    @SerializedName("list_club")
    public List<ListClub> listClub;

    public P2(String name, List<ListClub> listClub) {
        this.name = name;
        this.listClub = listClub;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ListClub> getListClub() {
        return listClub;
    }

    public void setListClub(List<ListClub> listClub) {
        this.listClub = listClub;
    }
}
