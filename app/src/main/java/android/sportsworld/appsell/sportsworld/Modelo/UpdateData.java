package android.sportsworld.appsell.sportsworld.Modelo;

public class UpdateData {
    public Boolean status;
    public String message;
    public Update data;

    public UpdateData(Boolean status, String message, Update data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Update getData() {
        return data;
    }

    public void setData(Update data) {
        this.data = data;
    }
}
