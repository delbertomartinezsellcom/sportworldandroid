package android.sportsworld.appsell.sportsworld.descripcion;

import android.Manifest;
import android.sportsworld.appsell.sportsworld.Globales.Globales;
import android.sportsworld.appsell.sportsworld.MenuPrincipalContenedor;
import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.sportsworld.appsell.sportsworld.GaleriaInfo.HorizontalAdapter;
import android.sportsworld.appsell.sportsworld.Mapa.ListClubsAdapter;
import android.sportsworld.appsell.sportsworld.Mapa.Mapa;
import android.sportsworld.appsell.sportsworld.MenuPrincipal;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.Club;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.Datum;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub_;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub__;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub___;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.P1;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.P2;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.P3;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.P4;
import android.sportsworld.appsell.sportsworld.Modelo.ClubsDatos.ClubDatos;
import android.sportsworld.appsell.sportsworld.Modelo.ClubsDatos.DataClubs;
import android.sportsworld.appsell.sportsworld.Noticias.Noticias;
import android.sportsworld.appsell.sportsworld.conectionManager.ConnectionManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;


import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageButton;
import android.widget.TextView;
import android.sportsworld.appsell.sportsworld.R;


import com.ToxicBakery.viewpager.transforms.RotateUpTransformer;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Main3Activity extends Fragment {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private GoogleMap mMap;
    private ViewPager mViewPager;
    private PageAdapter adapter;
    private ImageButton imgButton;
    private ImageButton goMapa;
    private LocationManager locationManager;
    private int idClub = 16;
    private TextView tituloSucursal;
    TabLayout tabLayout;

    public List<Datum> datum;
    public List<ListClub__> listClubs1;
    public List<ListClub> listClub2;
    public List<ListClub_> listClub3;
    public List<ListClub___> listClub4;

    public List<P4> p4;
    public List<P3> p3;
    public List<P2> p2;
    public List<P1> p1;

    public int elmascercano=0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.activity_main3, container, false);


        idClub = getArguments().getInt("idClub", 0);
        mViewPager = (ViewPager) v.findViewById(R.id.container);
        tituloSucursal = (TextView) v.findViewById(R.id.tituloSucursal);
        goMapa = (ImageButton) v.findViewById(R.id.goMapa);
        goMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goMapa = new Intent(getContext(), Mapa.class);
                startActivity(goMapa);
            }
        });

        imgButton = (ImageButton) v.findViewById(R.id.goMenu);
        imgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MenuPrincipal club = new MenuPrincipal();
                FragmentTransaction transactionClub = getActivity().getSupportFragmentManager().beginTransaction();
                transactionClub.replace(R.id.principal,club);
                //club.setArguments(bundle);
                transactionClub.addToBackStack(null);
                transactionClub.commit();
            }
        });
        // mViewPager.setPageTransformer(true, new RotateUpTransformer());
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        tabLayout = (TabLayout) v.findViewById(R.id.tabs);
        adapter = new PageAdapter(getFragmentManager(), tabLayout.getTabCount(), idClub);
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(getContext());
        progressDoalog.setMax(100);
        progressDoalog.setMessage("");
        progressDoalog.setTitle("Cargando información");
// show it
        progressDoalog.show();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://app.sportsworld.com.mx")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ConnectionManager servicio = retrofit.create(ConnectionManager.class);
        Call<ClubDatos> datosClub = servicio.getDatosClub(Integer.toString(idClub));
        datosClub.enqueue(new Callback<ClubDatos>() {
            @Override
            public void onResponse(Call<ClubDatos> call, Response<ClubDatos> response) {
                DataClubs clubDatos = response.body().getData();
                Log.d("adiosmundo", clubDatos.getDireccion());

                tituloSucursal.setText(clubDatos.getNombre());
                progressDoalog.dismiss();

            }

            @Override
            public void onFailure(Call<ClubDatos> call, Throwable t) {
                progressDoalog.dismiss();
                Log.d("adiosmundo", t.getLocalizedMessage().toString());

            }
        });
        tabLayout.getTabAt(1).select();
        TabLayout.Tab tabToSelect = tabLayout.getTabAt(1);
        tabToSelect.select();

    return v;
}
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}
