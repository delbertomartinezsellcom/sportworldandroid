package android.sportsworld.appsell.sportsworld.splashScreen;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.sportsworld.appsell.sportsworld.MainActivity;
import android.sportsworld.appsell.sportsworld.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.VideoView;


public class SplashScreen extends AppCompatActivity {

    VideoView videoHolder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        try{
            videoHolder = (VideoView)findViewById(R.id.videoview);
            //setContentView(videoHolder);

            Uri video = Uri.parse("android.resource://android.sportsworld.appsell.sportsworld/"+ R.raw.videosplash);
            videoHolder.setVideoURI(video);

            videoHolder.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                public void onCompletion(MediaPlayer mp) {
                    jump();
                }

            });
            videoHolder.start();
        } catch(Exception ex) {
            jump();
        }

    }

    private void jump() {
        if(isFinishing())
            return;
        startActivity(new Intent(this, MainActivity.class));
        videoHolder.stopPlayback();

        finish();
    }

    @Override
    protected void onDestroy() {
        // メディアプレーヤーを解放する
        if (videoHolder != null) {
            videoHolder.stopPlayback();
            videoHolder = null;
        }
        super.onDestroy();
    };
}
