package android.sportsworld.appsell.sportsworld.Mapa;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.sportsworld.appsell.sportsworld.MenuPrincipal;
import android.sportsworld.appsell.sportsworld.MenuPrincipalContenedor;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.Club;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub_;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub__;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub___;
import android.sportsworld.appsell.sportsworld.R;
import android.sportsworld.appsell.sportsworld.descripcion.Main3Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Callback;

public class ListClubsAdapter extends RecyclerView.Adapter<ListClubsAdapter.ViewHolder> {

    public List<ListClub__> listClub1;
    public List<ListClub> listClub2;
    public List<ListClub_> listClub3;
    public List<ListClub_> listClub4;
    public int club;
    private LayoutInflater mInflater;
    private ListClubsAdapter.ItemClickListener mClickListener;
    Context context;

    private ArrayList<ListClub__> arrayListSearch1;
    private ArrayList<ListClub> arrayListSearch2;
    private ArrayList<ListClub_> arrayListSearch3;
    private ArrayList<ListClub_> arrayListSearch4;


    public ListClubsAdapter(Context context,
                            List<ListClub__> listClub1,
                            List<ListClub> listClub2,
                            List<ListClub_> listClub3,
                            List<ListClub_> listClub4,
                            int club) {
        this.mInflater = LayoutInflater.from(context);
        this.listClub1 = listClub1;
        this.listClub2 = listClub2;
        this.listClub3 = listClub3;
        this.listClub4 = listClub4;


        this.arrayListSearch1 = new ArrayList<ListClub__>();
        this.arrayListSearch1.addAll(listClub1);

        this.arrayListSearch2 = new ArrayList<ListClub>();
        this.arrayListSearch2.addAll(listClub2);

        this.arrayListSearch3 = new ArrayList<ListClub_>();
        this.arrayListSearch3.addAll(listClub3);

        this.arrayListSearch4 = new ArrayList<ListClub_>();
        this.arrayListSearch4.addAll(listClub4);


        this.club = club;
        this.context = context;
    }

    @Override
    public ListClubsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recyclerview_maplist, parent, false);
        ListClubsAdapter.ViewHolder viewHolder = new ListClubsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ListClubsAdapter.ViewHolder holder, int position) {
        switch (club){
            case 1:{
                String tituloText = listClub1.get(position).getNombre();
                //String distanciaText = Integer.toString(listClub1.get(position).getDistance())+"KM";
                //String addressText = listClub1.get(position).getDireccion();
                int corazon = listClub1.get(position).getAgendar();

                if (corazon == 0){
                    holder.corazon.setImageResource(R.drawable.dislike_3x);
                }else{
                    holder.corazon.setImageResource(R.drawable.likeactive);
                }

                if (position == 0){
                    holder.franjaRoja.setVisibility(View.VISIBLE);
                }else{
                    holder.franjaRoja.setVisibility(View.INVISIBLE);
                }
                holder.titulo.setText(tituloText);
               // holder.distancia.setText(distanciaText);
               // holder.direccion.setText(addressText);
                break;
            }
            case 2:{
                String tituloText = listClub2.get(position).getNombre();
                //String distanciaText = Integer.toString(listClub2.get(position).getDistance())+"KM";
                //String addressText = listClub2.get(position).getDireccion();
                int corazon = listClub2.get(position).getAgendar();

                if (corazon == 0){
                    holder.corazon.setImageResource(R.drawable.dislike_3x);
                }else{
                    holder.corazon.setImageResource(R.drawable.likeactive);
                }

                if (position == 0){
                    holder.franjaRoja.setVisibility(View.VISIBLE);
                }else{
                    holder.franjaRoja.setVisibility(View.INVISIBLE);
                }
                holder.titulo.setText(tituloText);
                //holder.distancia.setText(distanciaText);
               // holder.direccion.setText(addressText);
                break;
            }
            case 3:{
                String tituloText = listClub3.get(position).getNombre();
               // String distanciaText = Integer.toString(listClub3.get(position).getDistance())+"KM";
                //String addressText = listClub3.get(position).getDireccion();
                int corazon = listClub3.get(position).getAgendar();

                if (corazon == 0){
                    holder.corazon.setImageResource(R.drawable.dislike_3x);
                }else{
                    holder.corazon.setImageResource(R.drawable.likeactive);
                }

                if (position == 0){
                    holder.franjaRoja.setVisibility(View.VISIBLE);
                }else{
                    holder.franjaRoja.setVisibility(View.INVISIBLE);
                }
                holder.titulo.setText(tituloText);
               // holder.distancia.setText(distanciaText);
               // holder.direccion.setText(addressText);
                break;

            }
            case 4:{
                String tituloText = listClub4.get(position).getNombre();
                //String distanciaText = Integer.toString(listClub4.get(position).getDistance())+"KM";
                //String addressText = listClub4.get(position).getDireccion();
                int corazon = listClub4.get(position).getAgendar();

                if (corazon == 0){
                    holder.corazon.setImageResource(R.drawable.dislike_3x);
                }else{
                    holder.corazon.setImageResource(R.drawable.likeactive);
                }

                if (position == 0){
                    holder.franjaRoja.setVisibility(View.VISIBLE);
                }else{
                    holder.franjaRoja.setVisibility(View.INVISIBLE);
                }
                holder.titulo.setText(tituloText);
                //holder.distancia.setText(distanciaText);
                //holder.direccion.setText(addressText);
                break;
            }

        }

    }


    @Override
    public int getItemCount() {
        switch (club){
            case 1: return listClub1.size();
            case 2: return listClub2.size();
            case 3: return listClub3.size();
            case 4: return listClub4.size();
            default:return 0;
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView titulo;
        public TextView distancia;
        public TextView direccion;
        public ImageView franjaRoja;
        public ImageView corazon;

        public ViewHolder(View itemView) {
            super(itemView);
            titulo = itemView.findViewById(R.id.titulo);
           // distancia = itemView.findViewById(R.id.distancia);
           // direccion = itemView.findViewById(R.id.address);
            franjaRoja = itemView.findViewById(R.id.franjaRoja);
            corazon = itemView.findViewById(R.id.corazon);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Log.d("clic","clic"+getAdapterPosition());
            switch (club){
                case 1:
                {
                    int id= listClub1.get(getAdapterPosition()).idun;
                    Intent mainClases = new Intent(context.getApplicationContext(), MenuPrincipalContenedor.class);
                    mainClases.putExtra("idClub", id);
                    mainClases.putExtra("openClass",true);
                    mainClases.putExtra("inMapa",true);
                    mainClases.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(mainClases);
                    break;
                }
                case 2:{
                    int id= listClub2.get(getAdapterPosition()).idun;
                    Intent mainClases = new Intent(context.getApplicationContext(), MenuPrincipalContenedor.class);
                    mainClases.putExtra("idClub", id);
                    mainClases.putExtra("openClass",true);
                    mainClases.putExtra("inMapa",true);
                    mainClases.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(mainClases);
                    break;

                }case 3:{
                    int id= listClub3.get(getAdapterPosition()).idun;
                    Intent mainClases = new Intent(context.getApplicationContext(), MenuPrincipalContenedor.class);
                    mainClases.putExtra("idClub", id);
                    mainClases.putExtra("openClass",true);
                    mainClases.putExtra("inMapa",true);
                    mainClases.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(mainClases);
                    break;

                }case 4:{
                    int id= listClub4.get(getAdapterPosition()).idun;
                    Intent mainClases = new Intent(context.getApplicationContext(), MenuPrincipalContenedor.class);
                    mainClases.putExtra("idClub", id);
                    mainClases.putExtra("openClass",true);
                    mainClases.putExtra("inMapa",true);
                    mainClases.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(mainClases);
                    break;
                }
            }
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position

    public Object getItem(int id) {
        switch (club) {
            case 1:
                return listClub1;
            case 2:
                return listClub2;
            case 3:
                return listClub3;
            case 4:
                return listClub4;
                default:return null;
        }
    }


    // allows clicks events to be caught
    public void setClickListener(ListClubsAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        switch (club) {
            case 1:
                listClub1.clear();
                if (charText.length() == 0) {
                    listClub1.addAll(arrayListSearch1);
                } else {
                    for (ListClub__ wp : arrayListSearch1) {
                        if (wp.getNombre().toLowerCase(Locale.getDefault()).contains(charText)) {
                            listClub1.add(wp);
                        }
                    }
                }
                notifyDataSetChanged();

            case 2:
                listClub2.clear();
                if (charText.length() == 0) {
                    listClub2.addAll(arrayListSearch2);
                } else {
                    for (ListClub wp : arrayListSearch2) {
                        if (wp.getNombre().toLowerCase(Locale.getDefault()).contains(charText)) {
                            listClub2.add(wp);
                        }
                    }
                }
                notifyDataSetChanged();

            case 3:
                listClub3.clear();
                if (charText.length() == 0) {
                    listClub3.addAll(arrayListSearch3);
                } else {
                    for (ListClub_ wp : arrayListSearch3) {
                        if (wp.getNombre().toLowerCase(Locale.getDefault()).contains(charText)) {
                            listClub3.add(wp);
                        }
                    }
                }
                notifyDataSetChanged();


            case 4:
                listClub4.clear();
                if (charText.length() == 0) {
                    listClub4.addAll(arrayListSearch4);
                } else {
                    for (ListClub_ wp : arrayListSearch4) {
                        if (wp.getNombre().toLowerCase(Locale.getDefault()).contains(charText)) {
                            listClub4.add(wp);
                        }
                    }
                }
                notifyDataSetChanged();


        }

    }

}

