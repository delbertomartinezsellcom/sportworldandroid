package android.sportsworld.appsell.sportsworld.Perfil;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.sportsworld.appsell.sportsworld.MainActivity;
import android.sportsworld.appsell.sportsworld.MenuPrincipalContenedor;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.Club;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.Datum;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub_;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub__;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub___;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.P1;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.P2;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.P3;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.P4;
import android.sportsworld.appsell.sportsworld.Modelo.RegistroRespuesta;
import android.sportsworld.appsell.sportsworld.Modelo.Success;
import android.sportsworld.appsell.sportsworld.Modelo.Update;
import android.sportsworld.appsell.sportsworld.Modelo.UpdateData;
import android.sportsworld.appsell.sportsworld.TerminosYCondiciones;
import android.sportsworld.appsell.sportsworld.conectionManager.ConnectionManager;
import android.sportsworld.appsell.sportsworld.registro.Registro;
import android.sportsworld.appsell.sportsworld.tools.CircleTransform;
import android.sportsworld.appsell.sportsworld.tools.CircleTransformGlide;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.sportsworld.appsell.sportsworld.R;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.Picasso;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class EditPerfil extends Activity implements AdapterView.OnItemSelectedListener{

    private ImageButton goMenu;
    private ImageButton imagenPhotoPerfil;
    private Button avisoprivacidad;
    private Button buttonRegistro;
    private SharedPreferences prefs;
    public static final int REQUEST_CODE_CAMERA = 0012;
    public static final int REQUEST_CODE_GALLERY = 0013;
    private String [] items = {"Camera","Gallery"};

    List<String> tiposMembresias;
    List<String> clubItems;
    Spinner tiposMembresiaSpinner;
    Spinner clubItemsSpinner;
    int positionPicker = 0;
    int positionPickerClub = 0;
    public List<Datum> datum;
    public List<ListClub__> listClubs1;
    public List<ListClub> listClub2;
    public List<ListClub_> listClub3;
    public List<ListClub___> listClub4;

    public List<P4> p4;
    public List<P3> p3;
    public List<P2> p2;
    public List<P1> p1;


    //Textos
    private TextView nombreText,membresiaText,mantenimientoText,edadText,alturaText,pesoText,emailText;
    String fotoBase64;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_edit_perfil);

        nombreText = (TextView) findViewById(R.id.nombreText);
        membresiaText = (TextView) findViewById(R.id.membresiaText);
        mantenimientoText = (TextView) findViewById(R.id.mantenimientoText);
        edadText = (TextView) findViewById(R.id.edadText);
        alturaText = (TextView) findViewById(R.id.alturaText);
        pesoText = (TextView) findViewById(R.id.pesoText);
        emailText = (TextView) findViewById(R.id.emailText);
        avisoprivacidad=(Button) findViewById(R.id.avisoprivacidad);
        buttonRegistro = (Button) findViewById(R.id.buttonRegistro);
        avisoprivacidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aviso = new Intent(EditPerfil.this, TerminosYCondiciones.class);
                startActivity(aviso);
            }
        });

        prefs = this.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        goMenu = (ImageButton) findViewById(R.id.goMenu);
        goMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainClases = new Intent(getApplicationContext(), MenuPrincipalContenedor.class);
                mainClases.putExtra("idClub", 16);
                mainClases.putExtra("openClass",false);
                mainClases.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(mainClases);
            }
        });

        imagenPhotoPerfil= (ImageButton) findViewById(R.id.imagenPhotoPerfil);
        final Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                String path = prefs.getString("profile_photo","");
                URL url = null;
                try {
                    url = new URL(path);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                File f = new File(url.getFile());

                byte bytes[] = new byte[0];
                try {
                    bytes = FileUtils.readFileToByteArray(f);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                fotoBase64 = Base64.encodeToString(bytes, Base64.DEFAULT);

                Picasso.get().load(path).priority(Picasso.Priority.LOW).fit().centerCrop().transform(new CircleTransform()).into(imagenPhotoPerfil);
            }
        });
        imagenPhotoPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //EasyImage.openCamera(getApplication(), 1);
                openImage();
            }
        });

        //Configuracion de spiners
         tiposMembresiaSpinner =(Spinner)findViewById(R.id.tipoMembresia);
         clubItemsSpinner = (Spinner)findViewById(R.id.clubText);
        tiposMembresias = new ArrayList<String>();
        tiposMembresias.add("Socio");
        tiposMembresias.add("Invitado");
        tiposMembresias.add("Empleado");
        tiposMembresias.add("Individual");
        clubItems = new ArrayList<String>();
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tiposMembresias);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        tiposMembresiaSpinner.setAdapter(dataAdapter);

        int seleccionTipoMembresia = 0;
        String tipoMembresiaPref = prefs.getString("member_type","");
        for (int i=0;i<tiposMembresias.size();i++){

            if(tipoMembresiaPref.equals(tiposMembresias.get(i))){
                seleccionTipoMembresia = i;
                Log.d("encontrado",tipoMembresiaPref + tiposMembresias.get(i));
            }
        }
        tiposMembresiaSpinner.setSelection(seleccionTipoMembresia);

        tiposMembresiaSpinner.setOnItemSelectedListener(this);

        //Recolectar datos clubs
        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(EditPerfil.this);
        progressDoalog.setMax(100);
        progressDoalog.setMessage("");
        progressDoalog.setTitle("Recolectando Clubs");
        progressDoalog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://app.sportsworld.com.mx")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ConnectionManager servicio =retrofit.create(ConnectionManager.class);
        Call<Club> clubs = servicio.getClubs(Double.toString(0.0),Double.toString(0.0));
        clubs.enqueue(new Callback<Club>() {
            @Override
            public void onResponse(Call<Club> call, retrofit2.Response<Club> response) {

                datum = new ArrayList<Datum>();
                datum = response.body().getData();

                p1 = new ArrayList<P1>();
                p2 = new ArrayList<P2>();
                p3 = new ArrayList<P3>();
                p4 = new ArrayList<P4>();

                p1 = datum.get(0).getP1();
                p2 = datum.get(0).getP2();
                p3 = datum.get(0).getP3();
                p4 = datum.get(0).getP4();

                listClubs1 = new ArrayList<ListClub__>();
                listClubs1 = p1.get(0).getListClub();
                List<ListClub> listClub2 = new ArrayList<ListClub>();
                listClub2 = p2.get(0).getListClub();
                List<ListClub_> listClub3 = new ArrayList<ListClub_>();
                listClub3 = p3.get(0).getListClub();
                List<ListClub_> listClub4 = new ArrayList<ListClub_>();
                listClub4 = p4.get(0).getListClub();


                for (int i = 0; i < listClub3.size(); i++) {
                    clubItems.add(listClub3.get(i).getNombre());
                }
                for (int i = 0; i < listClub4.size(); i++) {
                    clubItems.add(listClub4.get(i).getNombre());
                }
                // CarouselPicker.CarouselViewAdapter clubItemsAdapter = new CarouselPicker.CarouselViewAdapter(getBaseContext(), clubItems, 0);
                // carouselPickerClub.setAdapter(clubItemsAdapter);

                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, clubItems);
                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // attaching data adapter to spinner
                int cualclubSelect=0;
                String clubActual = prefs.getString("club","");
                for (int i=0;i<clubItems.size();i++){

                    if(clubActual.equals(clubItems.get(i))){
                        cualclubSelect = i;
                        Log.d("encontrado",clubActual + clubItems.get(i));
                    }
                }

                clubItemsSpinner.setAdapter(dataAdapter);
                clubItemsSpinner.setOnItemSelectedListener(EditPerfil.this);
                clubItemsSpinner.setSelection(cualclubSelect);
                progressDoalog.dismiss();
            }
            @Override
            public void onFailure(Call<Club> call, Throwable t) {
                Log.d("aqui",t.getLocalizedMessage().toString());
                progressDoalog.dismiss();
            }
        });


        //se llenan los datos de prof
        //Lleno datos
        nombreText.setText(prefs.getString("name",""));
        membresiaText.setText(Integer.toString(prefs.getInt("membernumber",0)));
        mantenimientoText.setText(prefs.getString("mainteniment",""));
        edadText.setText(Integer.toString(prefs.getInt("age",0)));
        alturaText.setText(Integer.toString(prefs.getInt("tallest",0)));
        pesoText.setText(Integer.toString(prefs.getInt("weight",0)));
        emailText.setText(prefs.getString("mail",""));


        buttonRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject json = new JSONObject();
                try{
                    json.put("height", Double.parseDouble(alturaText.getText().toString()));
                    json.put("weight", Double.parseDouble(pesoText.getText().toString()));
                    json.put("age", Integer.parseInt(edadText.getText().toString()));
                    json.put("user_id", prefs.getInt("user_id",0));
                    json.put("dob", "1983-05-03");
                    json.put("memunic_id", 0);
                    json.put("img",fotoBase64);
                }catch (JSONException e){
                    e.printStackTrace();
                }
                final ProgressDialog progressDoalog;
                progressDoalog = new ProgressDialog(EditPerfil.this);
                progressDoalog.setMax(100);
                progressDoalog.setMessage("");
                progressDoalog.setTitle("Actualizando Información");
// show it
                progressDoalog.show();
                RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(json.toString()));
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("https://app.sportsworld.com.mx")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                ConnectionManager updateRegistro = retrofit.create(ConnectionManager.class);
                final Call<UpdateData> usuario = updateRegistro.updateProfile(body);
                usuario.enqueue(new Callback<UpdateData>() {
                    @Override
                    public void onResponse(Call<UpdateData> call, Response<UpdateData> response) {
                        Update update = new Update();
                        update = response.body().getData();

                        Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_LONG).show();
                        if (response.body().getStatus()){
                            Log.d("profile_phoot",update.getProfilePhoto());
                            saveOnPreferences(
                                    update.getIdroutine(),
                                    update.getIdclub(),
                                    update.getUserid(),
                                    update.getName(),
                                    Integer.parseInt(update.getWeight()),
                                    update.getClub(),
                                    Integer.parseInt(update.getTallest()),
                                    update.getAge(),
                                    update.getMail(),
                                    update.getMemberType(),
                                    update.getDob(),
                                    update.getMainteiment(),
                                    update.getGender(),
                                    update.getMembernumber(),
                                    update.getProfilePhoto());
                        }
                        progressDoalog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<UpdateData> call, Throwable t) {
                        progressDoalog.dismiss();
                    }
                });

            }
        });

    }


    private void openImage(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Options");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(items[i].equals("Camera")){
                    EasyImage.openCamera(EditPerfil.this,REQUEST_CODE_CAMERA);
                }else if(items[i].equals("Gallery")){
                    EasyImage.openGallery(EditPerfil.this, REQUEST_CODE_GALLERY);
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void changeOption(Spinner spinner) {
        if (spinner.isEnabled()) {
            spinner.setEnabled(false);
        } else {
            spinner.setEnabled(true);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                switch (type){
                    case REQUEST_CODE_CAMERA:
                        Glide.with(EditPerfil.this)
                                .load(imageFile)
                                .centerCrop()
                                .transform(new CircleTransformGlide(getApplicationContext()))
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(imagenPhotoPerfil);

                        try {
                            byte bytes[] = FileUtils.readFileToByteArray(imageFile);
                            fotoBase64 = Base64.encodeToString(bytes, Base64.DEFAULT);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                        // tvPath.setText(imageFile.getAbsolutePath());
                        break;
                    case REQUEST_CODE_GALLERY:
                        Glide.with(EditPerfil.this)
                                .load(imageFile)
                                .centerCrop()
                                .transform(new CircleTransformGlide(getApplicationContext()))
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .into(imagenPhotoPerfil);
                        try {
                            byte bytes[] = FileUtils.readFileToByteArray(imageFile);
                            fotoBase64 = Base64.encodeToString(bytes, Base64.DEFAULT);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                       // tvPath.setText(imageFile.getAbsolutePath());
                        break;
                }
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spinner = (Spinner) parent;
       /* if (spinner.getId() == R.id.tipoMembresia){
            positionPicker = position;
        }else{
            positionPickerClub = position;
        }*/
        if (spinner.isEnabled()) {
            spinner.setEnabled(false);
        } else {
            spinner.setEnabled(true);

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    private void saveOnPreferences( int idroutine, int idclub,int user_id,
                                   String name,int weight,String club,int tallest,
                                   int age, String mail,String member_type,
                                   String dob,String mainteiment,String gender,
                                   int membernumber,String profile_photo){
        SharedPreferences.Editor editor= prefs.edit();
        editor.putString("profile_photo",profile_photo);
        editor.putInt("idroutine",idroutine);
        editor.putInt("idclub",idclub);
        editor.putInt("user_id",user_id);
        editor.putString("name",name);
        editor.putInt("weight",weight);
        editor.putString("club",club);
        editor.putInt("tallest",tallest);
        editor.putInt("age",age);
        editor.putString("member_type",member_type);
        editor.putString("mail",mail);
        editor.putString("dob",dob);
        editor.putString("mainteniment",mainteiment);
        editor.putString("gender",gender);
        editor.putInt("membernumber",membernumber);
        editor.putString("profile_photo_last",profile_photo);


        editor.apply();
    }
}
