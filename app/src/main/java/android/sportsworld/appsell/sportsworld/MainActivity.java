package android.sportsworld.appsell.sportsworld;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.provider.Settings;
import android.sportsworld.appsell.sportsworld.Globales.Globales;
import android.sportsworld.appsell.sportsworld.Modelo.Success;
import android.sportsworld.appsell.sportsworld.Modelo.Usuario;
import android.sportsworld.appsell.sportsworld.conectionManager.ConnectionManager;
import android.sportsworld.appsell.sportsworld.descripcion.Main3Activity;
import android.sportsworld.appsell.sportsworld.olvidastePassword.OlvidastePassword;
import android.sportsworld.appsell.sportsworld.registro.Registro;
import android.sportsworld.appsell.sportsworld.tools.AeSimpleSHA1;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;



import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import android.location.Location;

public class MainActivity extends AppCompatActivity  implements android.location.LocationListener,OnMapReadyCallback
         {

             private GoogleMap mMap;
    VideoView videoHolder;
    Button iniciarSesionBtn,olvidastecontrasenaBtn,registrarBtn,terminoscondiciones;
    EditText usuarioTxt,passwordTxt;
    private SharedPreferences prefs;
    private Location location;
    private LocationManager locationManager;
             SupportMapFragment mapFragment;

    @Override
    protected void onStart() {
        super.onStart();


        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        try{
            videoHolder = (VideoView)findViewById(R.id.videoView2);
            Uri video = Uri.parse("android.resource://android.sportsworld.appsell.sportsworld/"+ R.raw.videosinlogo);
            videoHolder.setVideoURI(video);
            display.getSize(size);
            videoHolder.getLayoutParams().width = display.getHeight()*2;
            videoHolder.getLayoutParams().height = display.getHeight()+80;
            Log.d("medidas",Integer.toString( videoHolder.getLayoutParams().width));
            videoHolder.setX(-display.getHeight()/2-150);
            videoHolder.setY(-150);

            videoHolder.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.setLooping(true);
                }
            });
            videoHolder.start();

        } catch(Exception ex) {
            //jump();
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

        try {
            int gpsSignal = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
            if (gpsSignal == 0) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
                onLocationChanged(location);
            }
        } catch (Exception e) {

        }

        prefs = this.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        iniciarSesionBtn = (Button)findViewById(R.id.buttonIniciarSesion);
        olvidastecontrasenaBtn =(Button)findViewById(R.id.olvidaste);
        terminoscondiciones = (Button) findViewById(R.id.terminoscondiciones);
        terminoscondiciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent terminos= new Intent(MainActivity.this,TerminosYCondiciones.class);
                startActivity(terminos);
            }
        });
        registrarBtn = (Button)findViewById(R.id.button2);
        olvidastecontrasenaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent olvidaste = new Intent(MainActivity.this,OlvidastePassword.class);
                startActivity(olvidaste);
            }
        });

        registrarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registro = new Intent(MainActivity.this,Registro.class);
                startActivity(registro);
            }
        });

        usuarioTxt = (EditText) findViewById(R.id.editNombre);
        passwordTxt = (EditText) findViewById(R.id.editContrasena);

        iniciarSesionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!usuarioTxt.toString().isEmpty() && !passwordTxt.toString().isEmpty() ) {
                    try {
                        String resultado = AeSimpleSHA1.SHA1(passwordTxt.getText().toString());

                        String dato = usuarioTxt.getText().toString().toLowerCase() + "." + resultado.toLowerCase();
                        byte[] data = dato.getBytes("UTF-8");
                        String base64 = Base64.encodeToString(data, Base64.NO_WRAP);
                        Log.d("result", base64);

                        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                                .connectTimeout(3, TimeUnit.MINUTES)
                                .readTimeout(30, TimeUnit.SECONDS)
                                .writeTimeout(15, TimeUnit.SECONDS)
                                .build();

                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl("https://app.sportsworld.com.mx/")
                                .client(okHttpClient)
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();

                        ConnectionManager servicio = retrofit.create(ConnectionManager.class);
                        Call<Success> usuario = servicio.usuarioLogin(base64);

                        final ProgressDialog progressDoalog;
                        progressDoalog = new ProgressDialog(MainActivity.this);
                        progressDoalog.setMax(100);
                        progressDoalog.setMessage("");
                        progressDoalog.setTitle("Iniciando sesión");
                        // show it
                        progressDoalog.show();

                        usuario.enqueue(new Callback<Success>() {
                            @Override
                            public void onResponse(Call<Success> call, Response<Success> response) {
//                            Log.d("result",response.body().toString());

                                Success user = response.body();

                                if (user.isStatus()) {
                                    saveOnPreferences(user.getData().getSecret_key(),user.getData().getIdroutine(),user.getData().getIdclub(),
                                            user.getData().getUser_id(),user.getData().getName(),user.getData().getWeight(),user.getData().getUsuario(),user.getData().getClub(),user.getData().getTallest(),user.getData().getAge(),user.getData().getMail(),user.getData().getMember_type(),user.getData().getRegister_date(),user.getData().getGender_id(),user.getData().getDob(),user.getData().getMainteinment(),user.getData().getGender(),user.getData().getMembernumber(),user.getData().getProfile_photo(),user.getData().getMemunic_id());

                                   // Toast.makeText(getApplicationContext(), "Usuario correcto", Toast.LENGTH_LONG).show();
                                    Intent goIndex = new Intent(getApplication(),MenuPrincipalContenedor.class);
                                    goIndex.putExtra("openClass",true);
                                    goIndex.putExtra("inicioSesion",true);
                                    goIndex.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    videoHolder.stopPlayback();
                                    videoHolder.clearAnimation();
                                    videoHolder.suspend(); // clears
                                    startActivity(goIndex);

                                } else {
                                    Toast.makeText(getApplicationContext(), "Usuario incorrecto", Toast.LENGTH_LONG).show();

                                }

                                progressDoalog.dismiss();
                            }

                            @Override
                            public void onFailure(Call<Success> call, Throwable t) {
                                Log.d("resultadoFalse", t.getMessage());

                                progressDoalog.dismiss();
                            }
                        });

                        Log.d("result", base64);

                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                }else{
                    Toast.makeText(getApplicationContext(), "Algún dato esta vacío", Toast.LENGTH_LONG).show();
                }
            }
        });


    }

    private void saveOnPreferences(String key, int idroutine, int idclub,int user_id,
                                   String name,int weight,String usuario,String club,int tallest,
                                   int age, String mail,String member_type,String register_day,
                                   int gender_id,String dob,String mainteiment,String gender,
                                   int membernumber,String profile_photo,int memunic_id){
        SharedPreferences.Editor editor= prefs.edit();
        editor.putInt("idroutine",idroutine);
        editor.putString("key",key);
        editor.putInt("idclub",idclub);
        editor.putInt("user_id",user_id);
        editor.putString("name",name);
        editor.putInt("weight",weight);
        editor.putString("usuario",usuario);
        editor.putString("club",club);
        editor.putInt("tallest",tallest);
        editor.putInt("age",age);
        editor.putString("mail",mail);
        editor.putString("member_type",member_type);
        editor.putString("register_day",register_day);
        editor.putInt("gender_id",gender_id);
        editor.putString("dob",dob);
        editor.putString("mainteniment",mainteiment);
        editor.putString("gender",gender);
        editor.putInt("membernumber",membernumber);
        editor.putString("profile_photo",profile_photo);
        editor.putInt("memunic_id",memunic_id);
        editor.apply();
    }


             @Override
             public void onLocationChanged(Location location) {
                 Globales.LATITUD = location.getLatitude();
                 Globales.LONGITUD = location.getLongitude();
                 //Toast.makeText(getApplicationContext(),Double.toString(location.getLongitude()),Toast.LENGTH_LONG).show();
             }

             @Override
             public void onStatusChanged(String provider, int status, Bundle extras) {

             }

             @Override
             public void onProviderEnabled(String provider) {

             }

             @Override
             public void onProviderDisabled(String provider) {

             }

             @Override
             public void onMapReady(GoogleMap googleMap) {
                 mMap = googleMap;
                 locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
                 if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                     // TODO: Consider calling
                     //    ActivityCompat#requestPermissions
                     // here to request the missing permissions, and then overriding
                     //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                     //                                          int[] grantResults)
                     // to handle the case where the user grants the permission. See the documentation
                     // for ActivityCompat#requestPermissions for more details.
                     return;
                 }
                 mMap.setMyLocationEnabled(true);
                 mMap.getUiSettings().setMyLocationButtonEnabled(false);

                 locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100000, 0,  this);
                 locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100000, 0,  this);
             }

             @Override
             protected void onPause() {
                 super.onPause();
                 locationManager.removeUpdates(this);
             }
         }
