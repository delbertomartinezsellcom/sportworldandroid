package android.sportsworld.appsell.sportsworld.Noticias;

import android.annotation.SuppressLint;
import android.content.Context;
import android.sportsworld.appsell.sportsworld.Modelo.ByPermanent;
import android.support.v4.app.Fragment;;
import android.os.Bundle;
import android.sportsworld.appsell.sportsworld.R;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;



public class NoticiasFragment extends Fragment {

    View view;
    ImageView noticiaImagen;
    TextView tituloNoticia;
    TextView contenidoNoticia;
    LinearLayout textoContenido;
    LinearLayout fragmentConetenedor;


    int pagina;

    private String noticiaImagenText;
    private String tituloNoticiaText;
    private String contenidoNoticiaText;
    private String textoContenidoText;



    public static NoticiasFragment newInstance(int pagina,String noticiaImagenText, String tituloNoticiaText, String contenidoNoticiaText, String textoContenidoText){
        NoticiasFragment fragmentNoticias = new NoticiasFragment();
        Bundle args = new Bundle();
        args.putInt("paginas",pagina);

        return fragmentNoticias;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_noticias, container, false);

        noticiaImagen = (ImageView) view.findViewById(R.id.imagenNoticia);
        tituloNoticia = (TextView) view.findViewById(R.id.tituloNoticia);
        contenidoNoticia = (TextView) view.findViewById(R.id.contenidoNoticia);
        textoContenido = (LinearLayout) view.findViewById(R.id.textoContenido);
        fragmentConetenedor = (LinearLayout) view.findViewById(R.id.contenedor);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}