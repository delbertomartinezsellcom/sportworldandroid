package android.sportsworld.appsell.sportsworld.Invitados;

import android.sportsworld.appsell.sportsworld.GaleriaInfo.InformacionFragmento;
import android.sportsworld.appsell.sportsworld.Modelo.Pases;
import android.sportsworld.appsell.sportsworld.descripcion.AreaFragment;
import android.sportsworld.appsell.sportsworld.descripcion.ClasesFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

public class PageInvitationAdapter extends FragmentStatePagerAdapter
    {

        private int numberOfTabs;

    public PageInvitationAdapter(FragmentManager fm,int numberOfTabs){
        super(fm);
        this.numberOfTabs = numberOfTabs;

    }


        @Override
        public Fragment getItem(int position){


        switch (position){
            case 0:
                return new DisponiblesFragment();
            case 1:
                return new RedimidosFragment();

        }
        return null;
    }

        @Override
        public int getCount(){
        return numberOfTabs;
    }
    }
