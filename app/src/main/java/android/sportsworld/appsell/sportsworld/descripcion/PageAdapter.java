package android.sportsworld.appsell.sportsworld.descripcion;

import android.os.Bundle;
import android.sportsworld.appsell.sportsworld.GaleriaInfo.InformacionFragmento;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by victor.lagunas on 22/02/18.
 */

public class PageAdapter extends FragmentStatePagerAdapter {

    private int numberOfTabs;
    private int idClub;


    public PageAdapter(FragmentManager fm, int numberOfTabs, int idClub){
        super(fm);
        this.numberOfTabs = numberOfTabs;
        this.idClub = idClub;
    }

    @Override
    public Fragment getItem(int position){


        switch (position){
            case 0:{
                Bundle bundle = new Bundle();
                bundle.putInt("idss",idClub);
                InformacionFragmento infFragmento = new InformacionFragmento();
                infFragmento.setArguments(bundle);
                return infFragmento;
            }

            case 1:

                Bundle bundle = new Bundle();
                bundle.putInt("idss",idClub);
                ClasesFragment infFragmento = new ClasesFragment();
                infFragmento.setArguments(bundle);
                return infFragmento;

            case 2:
                return new AreaFragment();
        }
        return null;
    }

    @Override
    public int getCount(){
        return numberOfTabs;
    }
}
