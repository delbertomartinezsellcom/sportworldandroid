package android.sportsworld.appsell.sportsworld.Invitados;

import android.content.Context;
import android.content.Intent;
import android.sportsworld.appsell.sportsworld.Modelo.Asignados;
import android.sportsworld.appsell.sportsworld.Modelo.Pases;
import android.sportsworld.appsell.sportsworld.R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

public class AdaptadorAsignadosList extends BaseAdapter {

    private Context context;
    private int layout;
    private List<Asignados> asignados;

    public AdaptadorAsignadosList(Context context,int layout,List<Asignados> asignados){
        this.context = context;
        this.layout = layout;
        this.asignados = asignados;
    }
    @Override
    public int getCount() {
        return this.asignados.size();
    }

    @Override
    public Object getItem(int position) {
        return this.asignados.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup viewGroup) {
        View v = convertView;
        LayoutInflater layoutInflater = LayoutInflater.from(this.context);
        v = layoutInflater.inflate(R.layout.list_invitadosasignados,null);

        Asignados asignado = asignados.get(position);

        TextView producto= (TextView)v.findViewById(R.id.producto);
        producto.setText(asignado.getProducto());

        TextView cuando= (TextView)v.findViewById(R.id.cuando);
        cuando.setText(asignado.getCuando());

        ImageButton imagenButton = (ImageButton)v.findViewById(R.id.email);
        imagenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent enviarInvitacion = new Intent(context.getApplicationContext(),EnviarInvitacion.class);
                context.startActivity(enviarInvitacion);
            }
        });

        return v;

    }
}
