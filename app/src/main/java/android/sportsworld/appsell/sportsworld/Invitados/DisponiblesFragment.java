package android.sportsworld.appsell.sportsworld.Invitados;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.sportsworld.appsell.sportsworld.Modelo.Asignados;
import android.sportsworld.appsell.sportsworld.Modelo.Invitaciones;
import android.sportsworld.appsell.sportsworld.Modelo.Pases;
import android.sportsworld.appsell.sportsworld.conectionManager.ConnectionManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.sportsworld.appsell.sportsworld.R;
import android.widget.ListView;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class DisponiblesFragment extends Fragment {

    private ListView listView;
    private List<Asignados> asignados;
    View view;
    private SharedPreferences prefs;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_disponibles, container, false);
        listView = (ListView) view.findViewById(R.id.lista);
        prefs = this.getActivity().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://sandbox.sportsworld.com.mx")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ConnectionManager servicio =retrofit.create(ConnectionManager.class);
        Call<Invitaciones> invitaciones = servicio.getInvitaciones(Integer.toString(prefs.getInt("memunic_id",0)));
        invitaciones.enqueue(new Callback<Invitaciones>() {
            @Override
            public void onResponse(Call<Invitaciones> call, Response<Invitaciones> response) {
                asignados = new ArrayList<Asignados>();
                asignados = response.body().getAsignados();
                AdaptadorAsignadosList adaptador = new AdaptadorAsignadosList(DisponiblesFragment.this.getContext(),R.layout.list_invitadosasignados,asignados);
                listView.setAdapter(adaptador);
            }

            @Override
            public void onFailure(Call<Invitaciones> call, Throwable t) {

            }
        });

        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

}
