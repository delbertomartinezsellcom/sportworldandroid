package android.sportsworld.appsell.sportsworld.Modelo;

public class EarnIt {
    String success;
    EarnItData data;

    public EarnIt(String success, EarnItData data) {
        this.success = success;
        this.data = data;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public EarnItData getData() {
        return data;
    }

    public void setData(EarnItData data) {
        this.data = data;
    }
}
