package android.sportsworld.appsell.sportsworld.Modelo.Clubs;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class P1 {
    public String name;
    @SerializedName("list_club")
    public List<ListClub__> listClub;

    public P1(String name, List<ListClub__> listClub) {
        this.name = name;
        this.listClub = listClub;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ListClub__> getListClub() {
        return listClub;
    }

    public void setListClub(List<ListClub__> listClub) {
        this.listClub = listClub;
    }
}
