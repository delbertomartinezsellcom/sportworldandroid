package android.sportsworld.appsell.sportsworld.Modelo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataNoticias {
    @SerializedName("by_permanent")
    public List<ByPermanent> byPermanent;
    @SerializedName("by_date")
    public List<Object> byDate;

    public List<ByPermanent> getByPermanent() {
        return byPermanent;
    }

    public void setByPermanent(List<ByPermanent> byPermanent) {
        this.byPermanent = byPermanent;
    }

    public List<Object> getByDate() {
        return byDate;
    }

    public void setByDate(List<Object> byDate) {
        this.byDate = byDate;
    }

    public DataNoticias(List<ByPermanent> byPermanent, List<Object> byDate) {
        this.byPermanent = byPermanent;
        this.byDate = byDate;
    }
    public DataNoticias(){

    }
}
