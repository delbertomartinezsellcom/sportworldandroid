package android.sportsworld.appsell.sportsworld.Videos;

import android.sportsworld.appsell.sportsworld.Modelo.VideoM;
import android.sportsworld.appsell.sportsworld.R;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterVideos extends RecyclerView.Adapter<AdapterVideos.ViewHolder> {

    private ArrayList<VideoM> mDataSet;

    public AdapterVideos(ArrayList<VideoM> mDataSet) {
        this.mDataSet = mDataSet;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.videocards, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        VideoM video = mDataSet.get(position);
        holder.mImagenVideos.setImageResource(video.getImagen());

    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView mImagenVideos;

        public ViewHolder(View itemView) {
            super(itemView);

            mImagenVideos = (ImageView) itemView.findViewById(R.id.imagenVideos);

        }
    }




}
