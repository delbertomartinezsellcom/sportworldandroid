package android.sportsworld.appsell.sportsworld.Videos;

import android.sportsworld.appsell.sportsworld.Modelo.VideoM;
import android.sportsworld.appsell.sportsworld.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class Videos extends AppCompatActivity {

    private RecyclerView mRecyclerView, recyclerView2, recyclerView3;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videos);


        mRecyclerView = (RecyclerView) findViewById(R.id.listavideos);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new AdapterVideos(getmDataSet());
        mRecyclerView.setAdapter(mAdapter);

        recyclerView2 = (RecyclerView) findViewById(R.id.videosUltimos);
        recyclerView2.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        recyclerView2.setLayoutManager(mLayoutManager);
        mAdapter = new AdapterVideos2(getData());
        recyclerView2.setAdapter(mAdapter);

        recyclerView3 = (RecyclerView) findViewById(R.id.videosFavoritos);
        recyclerView3.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        recyclerView3.setLayoutManager(mLayoutManager);
        mAdapter = new AdapterVideos2(getDataFav());
        recyclerView3.setAdapter(mAdapter);

    }

    private ArrayList<VideoM> getmDataSet(){
        ArrayList<VideoM> mDataSet = new ArrayList<>();
        mDataSet.add(new VideoM(R.drawable.group));
        mDataSet.add(new VideoM(R.drawable.group));
        mDataSet.add(new VideoM(R.drawable.group));
        mDataSet.add(new VideoM(R.drawable.group));
        mDataSet.add(new VideoM(R.drawable.group));

        return mDataSet;
    }

    private ArrayList<VideoM> getData(){
        ArrayList<VideoM> dataM = new ArrayList<>();
        dataM.add(new VideoM("Inbike",R.drawable.group));
        dataM.add(new VideoM("Inbike",R.drawable.group));
        dataM.add(new VideoM("Inbike",R.drawable.group));
        dataM.add(new VideoM("Inbike",R.drawable.group));
        dataM.add(new VideoM("Inbike",R.drawable.group));

        return dataM;
    }

    private ArrayList<VideoM> getDataFav(){
        ArrayList<VideoM> dataFav = new ArrayList<>();
        dataFav.add(new VideoM("Inbike",R.drawable.group));
        dataFav.add(new VideoM("Inbike",R.drawable.group));
        dataFav.add(new VideoM("Inbike",R.drawable.group));
        dataFav.add(new VideoM("Inbike",R.drawable.group));
        dataFav.add(new VideoM("Inbike",R.drawable.group));

        return dataFav;
    }
}
