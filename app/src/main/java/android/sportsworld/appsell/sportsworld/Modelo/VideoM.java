package android.sportsworld.appsell.sportsworld.Modelo;

public class VideoM {

    private String nombre;
    private int imagen;

    public VideoM(){

    }

    public VideoM(int imagen) {
        this.imagen = imagen;
    }

    public VideoM(String nombre, int imagen) {
        this.nombre = nombre;
        this.imagen = imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }
}
