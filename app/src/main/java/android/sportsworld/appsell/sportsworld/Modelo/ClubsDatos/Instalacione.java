package android.sportsworld.appsell.sportsworld.Modelo.ClubsDatos;

public class Instalacione {
    String descripcion;
    String rutaimagen;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getRutaimagen() {
        return rutaimagen;
    }

    public void setRutaimagen(String rutaimagen) {
        this.rutaimagen = rutaimagen;
    }

    public Instalacione(String descripcion, String rutaimagen) {
        this.descripcion = descripcion;
        this.rutaimagen = rutaimagen;
    }
}
