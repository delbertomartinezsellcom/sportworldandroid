package android.sportsworld.appsell.sportsworld.olvidastePassword;

import android.sportsworld.appsell.sportsworld.R;
import android.sportsworld.appsell.sportsworld.conectionManager.ConnectionManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OlvidastePassword extends AppCompatActivity {

    EditText password,editCorreoElectronico;
    Button enviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_olvidaste_password);


        editCorreoElectronico = (EditText) findViewById(R.id.editCorreoElectronico);
        enviar = (Button) findViewById(R.id.botonEnviar);

        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!editCorreoElectronico.toString().isEmpty()){

                    JSONObject json = new JSONObject();
                    try{
                        json.put("email",editCorreoElectronico.getText());
                    }catch (Exception e){
                        Log.d("holamundo",e.getLocalizedMessage().toString());
                    }
                    RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),json.toString());
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl("http://cloud.sportsworld.com.mx/")
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    ConnectionManager manager = retrofit.create(ConnectionManager.class);
                    Call<ResponseBody> usuario = manager.setRecoveryPassword(body);
                    usuario.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                            Toast.makeText(getApplicationContext(),response.message().toString(),Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {

                            Toast.makeText(getApplicationContext(),t.getMessage().toString(),Toast.LENGTH_LONG).show();
                        }
                    });

                }else {
                    Toast.makeText(getApplicationContext(), "Algún dato esta vacío", Toast.LENGTH_LONG).show();
                }

            }
        });


    }
}
