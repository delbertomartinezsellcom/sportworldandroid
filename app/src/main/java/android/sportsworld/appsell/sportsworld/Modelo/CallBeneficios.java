package android.sportsworld.appsell.sportsworld.Modelo;

import java.util.List;

public class CallBeneficios {

    public List<BeneficiosObject> beneficios;

    public List<BeneficiosObject> getBeneficios() {
        return beneficios;
    }

    public CallBeneficios(List<BeneficiosObject> beneficios) {
        this.beneficios = beneficios;
    }

    public void setBeneficios(List<BeneficiosObject> beneficios) {
        this.beneficios = beneficios;
    }
}
