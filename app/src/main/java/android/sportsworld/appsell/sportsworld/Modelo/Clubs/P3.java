package android.sportsworld.appsell.sportsworld.Modelo.Clubs;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class P3 {
    public String name;
    @SerializedName("list_club")
    public List<ListClub_> listClub;

    public P3(String name, List<ListClub_> listClub) {
        this.name = name;
        this.listClub = listClub;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ListClub_> getListClub() {
        return listClub;
    }

    public void setListClub(List<ListClub_> listClub) {
        this.listClub = listClub;
    }
}
