package android.sportsworld.appsell.sportsworld.descripcion;

import android.os.Bundle;
import android.sportsworld.appsell.sportsworld.R;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by victor.lagunas on 18/01/18.
 */

public class ItemFragment extends Fragment {

    public static ItemFragment newInstance() {
        Bundle args = new Bundle();
        ItemFragment fragment = new ItemFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_area, container, false);
        return view;
    }
}