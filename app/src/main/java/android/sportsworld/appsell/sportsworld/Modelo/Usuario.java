package android.sportsworld.appsell.sportsworld.Modelo;

/**
 * Created by victor.lagunas on 11/01/18.
 */

public class Usuario {

    int idroutine;
    int idclub;
    int user_id;
    String name;
    int weight;

    public Usuario(int idroutine, int idclub, int user_id, String name, int weight, String usuario, String club, int tallest, int age, String mail, String member_type, String register_date, int gender_id, String dob, String mainteinment, String gender, int membernumber, String secret_key, String profile_photo, int memunic_id) {
        this.idroutine = idroutine;
        this.idclub = idclub;
        this.user_id = user_id;
        this.name = name;
        this.weight = weight;
        this.usuario = usuario;
        this.club = club;
        this.tallest = tallest;
        this.age = age;
        this.mail = mail;
        this.member_type = member_type;
        this.register_date = register_date;
        this.gender_id = gender_id;
        this.dob = dob;
        this.mainteiment = mainteinment;
        this.gender = gender;
        this.membernumber = membernumber;
        this.secret_key = secret_key;
        this.profile_photo = profile_photo;
        this.memunic_id = memunic_id;
    }

    String usuario;
    String club;
    int tallest;
    int age;
    String mail;
    String member_type;
    String register_date;
    int gender_id;
    String dob;
    String mainteiment;
    String gender;
    int membernumber;
    String secret_key;
    String profile_photo;
    int memunic_id;


    public int getIdroutine() {
        return idroutine;
    }

    public void setIdroutine(int idroutine) {
        this.idroutine = idroutine;
    }

    public int getIdclub() {
        return idclub;
    }

    public void setIdclub(int idclub) {
        this.idclub = idclub;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public int getTallest() {
        return tallest;
    }

    public void setTallest(int tallest) {
        this.tallest = tallest;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMember_type() {
        return member_type;
    }

    public void setMember_type(String member_type) {
        this.member_type = member_type;
    }

    public String getRegister_date() {
        return register_date;
    }

    public void setRegister_date(String register_date) {
        this.register_date = register_date;
    }

    public int getGender_id() {
        return gender_id;
    }

    public void setGender_id(int gender_id) {
        this.gender_id = gender_id;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getMainteinment() {
        return mainteiment;
    }

    public void setMainteinment(String mainteinment) {
        this.mainteiment = mainteinment;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getMembernumber() {
        return membernumber;
    }

    public void setMembernumber(int membernumber) {
        this.membernumber = membernumber;
    }

    public String getSecret_key() {
        return secret_key;
    }

    public void setSecret_key(String secret_key) {
        this.secret_key = secret_key;
    }

    public String getProfile_photo() {
        return profile_photo;
    }

    public void setProfile_photo(String profile_photo) {
        this.profile_photo = profile_photo;
    }

    public int getMemunic_id() {
        return memunic_id;
    }

    public void setMemunic_id(int memunic_id) {
        this.memunic_id = memunic_id;
    }

}
