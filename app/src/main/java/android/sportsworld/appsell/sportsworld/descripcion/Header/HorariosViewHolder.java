package android.sportsworld.appsell.sportsworld.descripcion.Header;

import android.sportsworld.appsell.sportsworld.R;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class HorariosViewHolder extends RecyclerView.ViewHolder{
    // each data item is just a string in this case
    public TextView mTextView,profesorTextView,horario;

    public HorariosViewHolder(View itemView){
        super(itemView);

        mTextView = (TextView)itemView.findViewById(R.id.titulo);
        profesorTextView = (TextView) itemView.findViewById(R.id.texto);
        horario = (TextView) itemView.findViewById(R.id.horario);
    }

}

/*
*
* public class HeaderViewHolder extends RecyclerView.ViewHolder {
    public TextView headerTitle;
    public HeaderViewHolder(View itemView) {
        super(itemView);
        headerTitle = (TextView)itemView.findViewById(R.id.header_id);
    }
}

* */