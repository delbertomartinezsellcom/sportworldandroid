package android.sportsworld.appsell.sportsworld.descripcion.Header;

import android.sportsworld.appsell.sportsworld.R;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class HeaderViewHolder extends RecyclerView.ViewHolder {
    public TextView headerTitle;
    public TextView mTextView,profesorTextView,horario;
    public HeaderViewHolder(View itemView) {
        super(itemView);
        headerTitle = (TextView)itemView.findViewById(R.id.header_id);
        mTextView = (TextView)itemView.findViewById(R.id.titulo);
        profesorTextView = (TextView) itemView.findViewById(R.id.texto);
        horario = (TextView) itemView.findViewById(R.id.horario);
    }
}
