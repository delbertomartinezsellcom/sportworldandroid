package android.sportsworld.appsell.sportsworld.Invitados;

import android.content.Context;
import android.content.Intent;
import android.sportsworld.appsell.sportsworld.Modelo.Pases;
import android.sportsworld.appsell.sportsworld.R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

public class AdaptadorPasesList extends BaseAdapter {

    private Context context;
    private int layout;
    private List<Pases> pases;

    public AdaptadorPasesList(Context context,int layout,List<Pases> pases){
        this.context = context;
        this.layout = layout;
        this.pases = pases;
    }
    @Override
    public int getCount() {
        return this.pases.size();
    }

    @Override
    public Object getItem(int position) {
        return this.pases.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup viewGroup) {
        View v = convertView;
        LayoutInflater layoutInflater = LayoutInflater.from(this.context);
        v = layoutInflater.inflate(R.layout.list_invitadospases,null);

        Pases pase = pases.get(position);

        TextView titulo= (TextView)v.findViewById(R.id.titulo);
        titulo.setText(pase.getNombre());

        TextView fecha= (TextView)v.findViewById(R.id.fecha);
        fecha.setText(pase.getFinVigencia());
        ImageButton imagenButton = (ImageButton)v.findViewById(R.id.email);
        imagenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent enviarInvitacion = new Intent(context.getApplicationContext(),EnviarInvitacion.class);
                context.startActivity(enviarInvitacion);
            }
        });

        return v;

    }
}
