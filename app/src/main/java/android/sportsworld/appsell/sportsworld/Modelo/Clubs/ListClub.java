package android.sportsworld.appsell.sportsworld.Modelo.Clubs;

public class ListClub {
    public Double latitud;
    public int distance;
    public Object estadoAbr;
    public int agendar;
    public Object telefono;
    public int idestado;
    public String direccion;
    public String horario;
    public int idun;
    public String clave;
    public int dcount;
    public String ruta360;
    public String nombre;
    public String rutavideo;
    public int preventa;
    public String estado;

    public ListClub(Double latitud, int distance, Object estadoAbr, int agendar, Object telefono, int idestado, String direccion, String horario, int idun, String clave, int dcount, String ruta360, String nombre, String rutavideo, int preventa, String estado, Double longitud) {
        this.latitud = latitud;
        this.distance = distance;
        this.estadoAbr = estadoAbr;
        this.agendar = agendar;
        this.telefono = telefono;
        this.idestado = idestado;
        this.direccion = direccion;
        this.horario = horario;
        this.idun = idun;
        this.clave = clave;
        this.dcount = dcount;
        this.ruta360 = ruta360;
        this.nombre = nombre;
        this.rutavideo = rutavideo;
        this.preventa = preventa;
        this.estado = estado;
        this.longitud = longitud;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public Object getEstadoAbr() {
        return estadoAbr;
    }

    public void setEstadoAbr(Object estadoAbr) {
        this.estadoAbr = estadoAbr;
    }

    public int getAgendar() {
        return agendar;
    }

    public void setAgendar(int agendar) {
        this.agendar = agendar;
    }

    public Object getTelefono() {
        return telefono;
    }

    public void setTelefono(Object telefono) {
        this.telefono = telefono;
    }

    public int getIdestado() {
        return idestado;
    }

    public void setIdestado(int idestado) {
        this.idestado = idestado;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public int getIdun() {
        return idun;
    }

    public void setIdun(int idun) {
        this.idun = idun;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public int getDcount() {
        return dcount;
    }

    public void setDcount(int dcount) {
        this.dcount = dcount;
    }

    public String getRuta360() {
        return ruta360;
    }

    public void setRuta360(String ruta360) {
        this.ruta360 = ruta360;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRutavideo() {
        return rutavideo;
    }

    public void setRutavideo(String rutavideo) {
        this.rutavideo = rutavideo;
    }

    public int getPreventa() {
        return preventa;
    }

    public void setPreventa(int preventa) {
        this.preventa = preventa;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public Double longitud;
}
