package android.sportsworld.appsell.sportsworld;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.sportsworld.appsell.sportsworld.Globales.Globales;
import android.sportsworld.appsell.sportsworld.Modelo.ByPermanent;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.Club;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.Datum;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub_;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub__;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub___;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.P1;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.P2;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.P3;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.P4;
import android.sportsworld.appsell.sportsworld.Noticias.Noticias;
import android.sportsworld.appsell.sportsworld.conectionManager.ConnectionManager;
import android.sportsworld.appsell.sportsworld.descripcion.Main3Activity;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MenuPrincipalContenedor extends FragmentActivity {


    private int idClub;
    private Boolean openInLogin = false;
    private Boolean openInMapa = false;
    private Boolean openClass = false;
    private Boolean voyPerfil = false;
    private RelativeLayout esconder;
    BottomNavigationView navigation;
    public List<Datum> datum;
    public List<ListClub__> listClubs1;
    public List<ListClub> listClub2;
    public List<ListClub_> listClub3;
    public List<ListClub___> listClub4;

    public List<P4> p4;
    public List<P3> p3;
    public List<P2> p2;
    public List<P1> p1;
    private LocationManager locationManager;

    public int elmascercano=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_menu_principal_contenedor);
        esconder = (RelativeLayout) findViewById(R.id.esconder);
        idClub = getIntent().getIntExtra("idClub", 0);
        openInMapa = getIntent().getBooleanExtra("inMapa",false);
        openInLogin = getIntent().getBooleanExtra("inicioSesion",false);
        voyPerfil = getIntent().getBooleanExtra("voyMenu",false);

            if (voyPerfil == false) {
            if (openInMapa == true) {
                try {
                    deDondeVengo();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (idClub == 0) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("https://app.sportsworld.com.mx")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                ConnectionManager servicio = retrofit.create(ConnectionManager.class);
                Call<Club> clubs = servicio.getClubs(Double.toString(Globales.LATITUD), Double.toString(Globales.LONGITUD));
                final ProgressDialog progressDoalog;
                progressDoalog = new ProgressDialog(MenuPrincipalContenedor.this);
                progressDoalog.setMax(100);
                progressDoalog.setMessage("");
                progressDoalog.setTitle("Recolectando datos");
                // show it
                progressDoalog.show();


                clubs.enqueue(new Callback<Club>() {
                    @Override
                    public void onResponse(Call<Club> call, Response<Club> response) {
                        datum = new ArrayList<Datum>();
                        datum = response.body().getData();

                        p1 = new ArrayList<P1>();
                        p2 = new ArrayList<P2>();
                        p3 = new ArrayList<P3>();
                        p4 = new ArrayList<P4>();

                        p1 = datum.get(0).getP1();
                        p2 = datum.get(0).getP2();
                        p3 = datum.get(0).getP3();
                        p4 = datum.get(0).getP4();


                        listClubs1 = new ArrayList<ListClub__>();
                        listClubs1 = p1.get(0).getListClub();
                        List<ListClub> listClub2 = new ArrayList<ListClub>();
                        listClub2 = p2.get(0).getListClub();
                        List<ListClub_> listClub3 = new ArrayList<ListClub_>();
                        listClub3 = p3.get(0).getListClub();
                        List<ListClub_> listClub4 = new ArrayList<ListClub_>();
                        listClub4 = p4.get(0).getListClub();
                        float  aux = 190;
                        double Radius = 6371; //Radio de la tierra
                        for (int i=0; i<listClub3.size();i++ ){
                           double latB = listClub3.get(i).latitud;
                           double lnogB = listClub3.get(i).longitud;
                            double lat1 =  Globales.LATITUD / 1E6;
                            double lon1 = Globales.LONGITUD / 1E6;
                            double lat2 = latB / 1E6;
                            double lon2 = lnogB / 1E6;
                            double dLat = Math.toRadians(lat2-lat1);
                            double dLon = Math.toRadians(lon2-lon1);
                            double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon /2) * Math.sin(dLon/2);
                            double c = 2 * Math.asin(Math.sqrt(a));
                            float res = (float) (Radius * c);
                            Log.d("DISTANCIA :", "" + res);
                           if ( res < aux ) {
                               aux = res;
                               idClub = listClub3.get(i).getIdun();
                           }
                        }

                        openClass = getIntent().getBooleanExtra("openClass", false);
                        try {
                            deDondeVengo();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        progressDoalog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<Club> call, Throwable t) {
                        progressDoalog.dismiss();

                    }
                });
            }
        }
         navigation = (BottomNavigationView) findViewById(R.id.navigation);
         navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
         navigation.getMenu().getItem(0).setChecked(true);

    }

    public void deDondeVengo() throws InterruptedException {
        Bundle bundle = new Bundle();
        if (openClass){
            bundle.putInt("idClub", idClub);
            Main3Activity club = new Main3Activity();
            FragmentTransaction transactionClub = getSupportFragmentManager().beginTransaction();
            transactionClub.replace(R.id.principal,club);
            club.setArguments(bundle);
            transactionClub.addToBackStack(null);
            transactionClub.commit();
        }else{
            bundle.putInt("idClub", idClub);
            Main3Activity club = new Main3Activity();
            FragmentTransaction transactionClub = getSupportFragmentManager().beginTransaction();
            transactionClub.replace(R.id.principal,club);
            club.setArguments(bundle);
            transactionClub.addToBackStack(null);
            transactionClub.commit();
        }

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_clases:
                    Bundle bundle = new Bundle();
                    if (openClass){
                        bundle.putInt("idClub", idClub);
                    }else{
                        bundle.putInt("idClub", idClub);
                    }
                    Main3Activity club = new Main3Activity();
                    FragmentTransaction transactionClub = getSupportFragmentManager().beginTransaction();
                    transactionClub.replace(R.id.principal,club);
                    club.setArguments(bundle);
                    transactionClub.addToBackStack(null);
                    transactionClub.commit();
                    return true;

                case R.id.navigation_noticias:
                    Noticias noticiasFragment = new Noticias();
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

                    transaction.replace(R.id.principal,noticiasFragment);
                    transaction.addToBackStack(null);

                    transaction.commit();
                    return true;
            }
            return false;
        }
    };


}



