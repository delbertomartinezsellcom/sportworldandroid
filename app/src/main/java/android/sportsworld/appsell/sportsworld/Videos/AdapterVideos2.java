package android.sportsworld.appsell.sportsworld.Videos;

import android.sportsworld.appsell.sportsworld.Modelo.VideoM;
import android.sportsworld.appsell.sportsworld.R;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterVideos2 extends RecyclerView.Adapter<AdapterVideos2.ViewHolder>{

    private ArrayList<VideoM> data;

    public AdapterVideos2(ArrayList<VideoM> data) {
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cardsvideosynombres, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        VideoM vid = data.get(position);
        holder.mImagenVideos.setImageResource(vid.getImagen());
        holder.mNombreVideo.setText(vid.getNombre());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView mImagenVideos;
        TextView mNombreVideo;


        public ViewHolder(View itemView) {
            super(itemView);

            mImagenVideos = (ImageView) itemView.findViewById(R.id.imagenVideos);
            mNombreVideo = (TextView) itemView.findViewById(R.id.nombreVideo);
        }

    }


}
