package android.sportsworld.appsell.sportsworld.Noticias;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.sportsworld.appsell.sportsworld.Modelo.ByPermanent;
import android.sportsworld.appsell.sportsworld.Modelo.Pases;
import android.sportsworld.appsell.sportsworld.R;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;


public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {


    private int layout;
    private List<ByPermanent> byPermanents;
    private OnItemClickListener itemClickListener;

    private Context context;


    public Adapter(List<ByPermanent> byPermanents, int layout, OnItemClickListener listener) {
        this.byPermanents = byPermanents;
        this.layout = layout;
        this.itemClickListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Inflamos el layout y se lo pasamos al constructor del ViewHolder, donde manejaremos
        // toda la lógica como extraer los datos, referencias...
        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        context = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // Llamamos al método Bind del ViewHolder pasándole objeto y listener
        holder.bind(byPermanents.get(position), itemClickListener);
    }

    @Override
    public int getItemCount() {
        return byPermanents.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // Elementos UI a rellenar
        public TextView titulo;
        public TextView fecha;
        public TextView subt;
        public ImageView imagenNoticia;
        public TextView tituloNoticia;
        public TextView contenidoNoticia;

        public ViewHolder(View itemView) {
            // Recibe la View completa. La pasa al constructor padre y enlazamos referencias UI
            // con nuestras propiedades ViewHolder declaradas justo arriba.
            super(itemView);
            titulo = (TextView) itemView.findViewById(R.id.titulo);
            fecha = (TextView) itemView.findViewById(R.id.subtitulo);
            subt = (TextView) itemView.findViewById(R.id.subt);
            tituloNoticia = (TextView) itemView.findViewById(R.id.tituloNoticia);
            contenidoNoticia= (TextView) itemView.findViewById(R.id.contenidoNoticia);
            imagenNoticia = (ImageView) itemView.findViewById(R.id.imagenNoticia);
        }

        public void bind(final ByPermanent byPermanents, final OnItemClickListener listener) {
            // Procesamos los datos a renderizar
            titulo.setText(byPermanents.getTitulo());
            fecha.setText(byPermanents.getIniciovigencia());
            subt.setText(byPermanents.getColumnista());
            tituloNoticia.setText(byPermanents.getTitulo());
            contenidoNoticia.setText(byPermanents.getResumen());
            if (byPermanents.getRutaimagen().isEmpty()) {
                imagenNoticia.setImageResource(R.drawable.img_infoclub_3x);
            }else{
                Picasso.get().load(byPermanents.getRutaimagen()).into(imagenNoticia);
            }
            //Picasso.with(context).get().load(movie.getPoster()).fit().into(imageViewPoster);
            // imageViewPoster.setImageResource(movie.getPoster());
            // Definimos que por cada elemento de nuestro recycler view, tenemos un click listener
            // que se comporta de la siguiente manera...
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(byPermanents, getAdapterPosition());
                }
            });
        }
    }

    // Declaramos nuestra interfaz con el/los método/s a implementar
    public interface OnItemClickListener {
        void onItemClick(ByPermanent movie, int position);
    }
}
