package android.sportsworld.appsell.sportsworld;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.sportsworld.appsell.sportsworld.Invitados.Invitados;
import android.sportsworld.appsell.sportsworld.Seguros.Seguros;
import android.sportsworld.appsell.sportsworld.beneficios.Beneficios;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class MiCredencial extends AppCompatActivity {

    private ImageView imagenProfile;
    private SharedPreferences prefs;
    private TextView nombreCliente;
    private TextView club;
    private TextView membresiaId;
    private Button editPasesDeInvitado;
    private Button beneficiosButton;
    private Button editSeguros;
    private ImageButton comeback;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mi_credencial);
        prefs = this.getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        imagenProfile = (ImageView) findViewById(R.id.fotoPerfil);
        comeback = (ImageButton)findViewById(R.id.comeback);
        nombreCliente = (TextView)findViewById(R.id.nombreCliente);
        club = (TextView) findViewById(R.id.clubBase);
        beneficiosButton = (Button) findViewById(R.id.editBeneficios);
        membresiaId= (TextView)findViewById(R.id.membresiaId);
        editSeguros = (Button) findViewById(R.id.editSeguros);

        editPasesDeInvitado = (Button) findViewById(R.id.editPasesDeInvitado);

        editPasesDeInvitado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent invitador = new Intent(getApplicationContext(), Invitados.class);
                startActivity(invitador);
            }
        });

        comeback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainClases = new Intent(getApplication().getApplicationContext(), MenuPrincipalContenedor.class);
                mainClases.putExtra("idClub", 16);
                mainClases.putExtra("openClass",false);
                mainClases.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(mainClases);
            }
        });

        beneficiosButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent beneficios = new Intent(getApplicationContext(), Beneficios.class);
                startActivity(beneficios);
            }
        });

        editSeguros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent seguros = new Intent(getApplicationContext(), Seguros.class);
                startActivity(seguros);
            }
        });
        Picasso.get().load(prefs.getString("profile_photo","")).resize(200,200).into(imagenProfile);
        nombreCliente.setText(prefs.getString("name",""));
        club.setText(prefs.getString("club",""));
        membresiaId.setText(Integer.toString(prefs.getInt("membernumber", 0)));


    }
}
