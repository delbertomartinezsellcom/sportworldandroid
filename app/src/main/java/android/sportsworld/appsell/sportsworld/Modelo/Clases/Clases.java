package android.sportsworld.appsell.sportsworld.Modelo.Clases;

import java.util.List;

public class Clases {

    public Boolean status;
    public String message;
    public List<DatumClases> data;

    public Clases(Boolean status, String message, List<DatumClases> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DatumClases> getData() {
        return data;
    }

    public void setData(List<DatumClases> data) {
        this.data = data;
    }
}
