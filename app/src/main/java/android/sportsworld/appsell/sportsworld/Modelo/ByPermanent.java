package android.sportsworld.appsell.sportsworld.Modelo;

public class ByPermanent {

    public int permanente;
    public String columnista;
    public int idpersona;
    public int idun;
    public String rutaimagen;
    public String descripcion;
    public int vigentehoy;
    public int idnoticias;
    public String finvigencia;
    public String resumen;
    public String iniciovigencia;
    public int idnoticiascategoria;
    public String categorianoticia;
    public String titulo;

    public ByPermanent(int permanente, String columnista, int idpersona, int idun, String rutaimagen, String descripcion, int vigentehoy, int idnoticias, String finvigencia, String resumen, String iniciovigencia, int idnoticiascategoria, String categorianoticia, String titulo) {
        this.permanente = permanente;
        this.columnista = columnista;
        this.idpersona = idpersona;
        this.idun = idun;
        this.rutaimagen = rutaimagen;
        this.descripcion = descripcion;
        this.vigentehoy = vigentehoy;
        this.idnoticias = idnoticias;
        this.finvigencia = finvigencia;
        this.resumen = resumen;
        this.iniciovigencia = iniciovigencia;
        this.idnoticiascategoria = idnoticiascategoria;
        this.categorianoticia = categorianoticia;
        this.titulo = titulo;
    }

    public int getPermanente() {
        return permanente;
    }

    public void setPermanente(int permanente) {
        this.permanente = permanente;
    }

    public String getColumnista() {
        return columnista;
    }

    public void setColumnista(String columnista) {
        this.columnista = columnista;
    }

    public int getIdpersona() {
        return idpersona;
    }

    public void setIdpersona(int idpersona) {
        this.idpersona = idpersona;
    }

    public int getIdun() {
        return idun;
    }

    public void setIdun(int idun) {
        this.idun = idun;
    }

    public String getRutaimagen() {
        return rutaimagen;
    }

    public void setRutaimagen(String rutaimagen) {
        this.rutaimagen = rutaimagen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getVigentehoy() {
        return vigentehoy;
    }

    public void setVigentehoy(int vigentehoy) {
        this.vigentehoy = vigentehoy;
    }

    public int getIdnoticias() {
        return idnoticias;
    }

    public void setIdnoticias(int idnoticias) {
        this.idnoticias = idnoticias;
    }

    public String getFinvigencia() {
        return finvigencia;
    }

    public void setFinvigencia(String finvigencia) {
        this.finvigencia = finvigencia;
    }

    public String getResumen() {
        return resumen;
    }

    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

    public String getIniciovigencia() {
        return iniciovigencia;
    }

    public void setIniciovigencia(String iniciovigencia) {
        this.iniciovigencia = iniciovigencia;
    }

    public int getIdnoticiascategoria() {
        return idnoticiascategoria;
    }

    public void setIdnoticiascategoria(int idnoticiascategoria) {
        this.idnoticiascategoria = idnoticiascategoria;
    }

    public String getCategorianoticia() {
        return categorianoticia;
    }

    public void setCategorianoticia(String categorianoticia) {
        this.categorianoticia = categorianoticia;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
}
