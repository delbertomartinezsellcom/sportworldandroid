package android.sportsworld.appsell.sportsworld.Modelo;

import com.google.gson.annotations.SerializedName;

public class Update {

    int idroutine;
    int idclub;
    @SerializedName("user_id")
    int userid;
    String name;
    String weight;
    String club;
    String gender;
    int age;
    String dob;
    String mainteiment;
    String mail;
    int membernumber;
    @SerializedName("profile_photo")
    String profilePhoto;
    String tallest;
    @SerializedName("member_type")
    String memberType;

    public Update(){

    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    public int getIdroutine() {
        return idroutine;
    }

    public void setIdroutine(int idroutine) {
        this.idroutine = idroutine;
    }

    public int getIdclub() {
        return idclub;
    }

    public void setIdclub(int idclub) {
        this.idclub = idclub;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getMainteiment() {
        return mainteiment;
    }

    public void setMainteiment(String mainteiment) {
        this.mainteiment = mainteiment;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public int getMembernumber() {
        return membernumber;
    }

    public void setMembernumber(int membernumber) {
        this.membernumber = membernumber;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public String getTallest() {
        return tallest;
    }

    public void setTallest(String tallest) {
        this.tallest = tallest;
    }

    public Update(int idroutine, int idclub, int userid, String name, String weight, String club, String gender, int age, String dob, String mainteiment, String mail, int membernumber, String profilePhoto, String tallest, String memberType) {
        this.idroutine = idroutine;
        this.idclub = idclub;
        this.userid = userid;
        this.name = name;
        this.weight = weight;
        this.club = club;
        this.gender = gender;
        this.age = age;
        this.dob = dob;
        this.mainteiment = mainteiment;
        this.mail = mail;
        this.membernumber = membernumber;
        this.profilePhoto = profilePhoto;
        this.tallest = tallest;
        this.memberType = memberType;
    }
}
