package android.sportsworld.appsell.sportsworld.Modelo.Clubs;

import android.widget.LinearLayout;

import java.util.List;

public class Datum {
    private List<P4> p4;
    private List<P3> p3;
    private List<P2> p2;
    private List<P1> p1;

    public Datum(){

    }
    public Datum(List<P4> p4, List<P3> p3, List<P2> p2, List<P1> p1) {
        this.p4 = p4;
        this.p3 = p3;
        this.p2 = p2;
        this.p1 = p1;
    }

    public List<P4> getP4() {
        return p4;
    }

    public void setP4(List<P4> p4) {
        this.p4 = p4;
    }

    public List<P3> getP3() {
        return p3;
    }

    public void setP3(List<P3> p3) {
        this.p3 = p3;
    }

    public List<P2> getP2() {
        return p2;
    }

    public void setP2(List<P2> p2) {
        this.p2 = p2;
    }

    public List<P1> getP1() {
        return p1;
    }

    public void setP1(List<P1> p1) {
        this.p1 = p1;
    }
}
