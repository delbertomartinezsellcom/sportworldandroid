package android.sportsworld.appsell.sportsworld.Modelo.Clases;

public class DatumClases {

    public int idinstalacionactividadprogramada;

    public String club;
    public String salon;
    public int idsalon;
    public String clase;
    public int idclase;
    public String instructor;
    public String inicio;
    public String fin;
    public String iniciovigencia;



    public String finvigencia;
    public int capacidadideal;
    public int capacidadmaxima;
    public int capacidadregistrada;
    public int reservacion;
    public int confirmados;
    public int agendar;
    public int demand;
    public Boolean inscrito;
    public boolean isHeader;

    public boolean getHeader() {
        return isHeader;
    }

    public void setHeader(Boolean header) {
        isHeader = header;
    }

    public DatumClases(){

    }

    public DatumClases(int idinstalacionactividadprogramada, String club, String salon, int idsalon, String clase, int idclase, String instructor, String inicio, String fin, String iniciovigencia, String finvigencia, int capacidadideal, int capacidadmaxima, int capacidadregistrada, int reservacion, int confirmados, int agendar, int demand, Boolean inscrito) {
        this.isHeader = false;
        this.idinstalacionactividadprogramada = idinstalacionactividadprogramada;
        this.club = club;
        this.salon = salon;
        this.idsalon = idsalon;
        this.clase = clase;
        this.idclase = idclase;
        this.instructor = instructor;
        this.inicio = inicio;
        this.fin = fin;
        this.iniciovigencia = iniciovigencia;
        this.finvigencia = finvigencia;
        this.capacidadideal = capacidadideal;
        this.capacidadmaxima = capacidadmaxima;
        this.capacidadregistrada = capacidadregistrada;
        this.reservacion = reservacion;
        this.confirmados = confirmados;
        this.agendar = agendar;
        this.demand = demand;
        this.inscrito = inscrito;
    }

    public String getIniciovigencia() {
        return iniciovigencia;
    }

    public void setIniciovigencia(String iniciovigencia) {
        this.iniciovigencia = iniciovigencia;
    }
    public int getIdinstalacionactividadprogramada() {
        return idinstalacionactividadprogramada;
    }

    public void setIdinstalacionactividadprogramada(int idinstalacionactividadprogramada) {
        this.idinstalacionactividadprogramada = idinstalacionactividadprogramada;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public String getSalon() {
        return salon;
    }

    public void setSalon(String salon) {
        this.salon = salon;
    }

    public int getIdsalon() {
        return idsalon;
    }

    public void setIdsalon(int idsalon) {
        this.idsalon = idsalon;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public int getIdclase() {
        return idclase;
    }

    public void setIdclase(int idclase) {
        this.idclase = idclase;
    }

    public String getInstructor() {
        return instructor;
    }

    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }

    public String getInicio() {
        return inicio;
    }

    public void setInicio(String inicio) {
        this.inicio = inicio;
    }

    public String getFin() {
        return fin;
    }

    public void setFin(String fin) {
        this.fin = fin;
    }

    public String getFinvigencia() {
        return finvigencia;
    }

    public void setFinvigencia(String finvigencia) {
        this.finvigencia = finvigencia;
    }

    public int getCapacidadideal() {
        return capacidadideal;
    }

    public void setCapacidadideal(int capacidadideal) {
        this.capacidadideal = capacidadideal;
    }

    public int getCapacidadmaxima() {
        return capacidadmaxima;
    }

    public void setCapacidadmaxima(int capacidadmaxima) {
        this.capacidadmaxima = capacidadmaxima;
    }

    public int getCapacidadregistrada() {
        return capacidadregistrada;
    }

    public void setCapacidadregistrada(int capacidadregistrada) {
        this.capacidadregistrada = capacidadregistrada;
    }

    public int getReservacion() {
        return reservacion;
    }

    public void setReservacion(int reservacion) {
        this.reservacion = reservacion;
    }

    public int getConfirmados() {
        return confirmados;
    }

    public void setConfirmados(int confirmados) {
        this.confirmados = confirmados;
    }

    public int getAgendar() {
        return agendar;
    }

    public void setAgendar(int agendar) {
        this.agendar = agendar;
    }

    public int getDemand() {
        return demand;
    }

    public void setDemand(int demand) {
        this.demand = demand;
    }

    public Boolean getInscrito() {
        return inscrito;
    }

    public void setInscrito(Boolean inscrito) {
        this.inscrito = inscrito;
    }
}
