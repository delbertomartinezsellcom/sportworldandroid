package android.sportsworld.appsell.sportsworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

public class TerminosYCondiciones extends AppCompatActivity {


    public TextView texto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terminos_ycondiciones);

        texto = (TextView) findViewById(R.id.terminos);
        texto.setMovementMethod(new ScrollingMovementMethod());
    }
}
