package android.sportsworld.appsell.sportsworld.descripcion;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.sportsworld.appsell.sportsworld.Modelo.Clases.DatumClases;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.Datum;
import android.sportsworld.appsell.sportsworld.Modelo.ReservacionClase;
import android.sportsworld.appsell.sportsworld.R;
import android.sportsworld.appsell.sportsworld.conectionManager.ConnectionManager;
import android.sportsworld.appsell.sportsworld.descripcion.Header.HeaderViewHolder;
import android.sportsworld.appsell.sportsworld.descripcion.Header.HorariosViewHolder;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicIntegerArray;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by victor.lagunas on 28/02/18.
 */

public class AdaptadorClases extends RecyclerView.Adapter<RecyclerView.ViewHolder>   {

    public List<DatumClases> datosClases;
    public List<DatumClases> datosClasesHeaders;
    private ItemClickListener mClickListener;

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;




    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    /*
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        // each data item is just a string in this case
        @Override
        public void onClick(View v) {
            SharedPreferences prefs = v.getContext().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
            JSONObject json = new JSONObject();

            try{
                json.put("classdate",datosClases.get(getAdapterPosition()).getInicio());
                json.put("user_id",Integer.toString(prefs.getInt("user_id",0)));
                json.put("employed_id",0);
                json.put("confirm",0);
                json.put("idconfirm",1);
                json.put("idinstactprg",datosClases.get(getAdapterPosition()).getIdinstalacionactividadprogramada());
            }catch (Exception e){
                Log.d("error",e.getLocalizedMessage().toString());
            }

            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),json.toString());
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://prepago.sportsworld.com.mx")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            ConnectionManager manager = retrofit.create(ConnectionManager.class);
            final Call<ReservacionClase> reservacion = manager.doReservation(body);
            reservacion.enqueue(new Callback<ReservacionClase>() {
                @Override
                public void onResponse(Call<ReservacionClase> call, Response<ReservacionClase> response) {
                    ReservacionClase reservacionClase = response.body();
                    Toast.makeText(itemView.getContext(),reservacionClase.getMessage(),Toast.LENGTH_LONG).show();
                }

                @Override
                public void onFailure(Call<ReservacionClase> call, Throwable t) {

                }
            });

            if (mClickListener != null) mClickListener.onItemClick(v, getAdapterPosition());
        }
    }
*/
    // Provide a suitable constructor (depends on the kind of dataset)
    public AdaptadorClases(List<DatumClases> datosClases) {

        this.datosClases = datosClases;
        ArrayList<String>horas = new ArrayList<>();
        //horas.add(this.datosClases.get(0).getInicio());
        Log.d("Volvi","volvi!!"+Integer.toString(datosClases.size()));
        for (int i = 0; i < datosClases.size(); i++) {
            if (horas.contains(datosClases.get(i).getInicio())){

            }else{
                horas.add(datosClases.get(i).getInicio());
                DatumClases datum = new DatumClases();
                datum = datosClases.get(i);
                datum.isHeader = true;
                this.datosClases.set(i,datum);
                Log.d("posicion",Integer.toString(i));
            }
        }
        Log.d("Volvi","Regrese!!"+Integer.toString(datosClases.size()));
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

       if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_layout, parent, false);

            return new HeaderViewHolder(view);
        } else if (viewType == TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.texto_horarios, parent, false);
            return new HorariosViewHolder(view);
        }
        throw new RuntimeException("No match for " + viewType + ".");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof  HeaderViewHolder){
            ((HeaderViewHolder) holder).headerTitle.setText(datosClases.get(position).getInicio());
            if (datosClases.get(position).getInscrito()) {

                ((HeaderViewHolder) holder).mTextView.setText(datosClases.get(position).getClase());
                ((HeaderViewHolder) holder).profesorTextView.setText(datosClases.get(position).getInstructor());
                String hora;
                if (Integer.parseInt(datosClases.get(position).getInicio().substring(0, 2)) < 12) {
                    hora = datosClases.get(position).getInicio() + " AM";
                } else {
                    hora = datosClases.get(position).getInicio() + " PM";
                }
                ((HeaderViewHolder) holder).horario.setText(hora);
                ((HeaderViewHolder) holder).mTextView.setTextColor(Color.parseColor("##5BC236"));
                ((HeaderViewHolder) holder).profesorTextView.setTextColor(Color.parseColor("##5BC236"));
                ((HeaderViewHolder) holder).horario.setTextColor(Color.parseColor("##5BC236"));
            } else {
                ((HeaderViewHolder) holder).mTextView.setText(datosClases.get(position).getClase());
                ((HeaderViewHolder) holder).profesorTextView.setText(datosClases.get(position).getInstructor());
                String hora;
                if (Integer.parseInt(datosClases.get(position).getInicio().substring(0, 2)) < 12) {
                    hora = datosClases.get(position).getInicio() + " AM";
                } else {
                    hora = datosClases.get(position).getInicio() + " PM";
                }
                ((HeaderViewHolder) holder).horario.setText(hora);
                ((HeaderViewHolder) holder).mTextView.setTextColor(Color.WHITE);
                ((HeaderViewHolder) holder).profesorTextView.setTextColor(Color.WHITE);
                ((HeaderViewHolder) holder).horario.setTextColor(Color.WHITE);

            }
        }else if (holder instanceof  HorariosViewHolder) {
        Log.d("POSICION:",Integer.toString(position));
            if (datosClases.get(position).getInscrito()) {

                ((HorariosViewHolder) holder).mTextView.setText(datosClases.get(position).getClase());
                ((HorariosViewHolder) holder).profesorTextView.setText(datosClases.get(position).getInstructor());
                String hora;
                if (Integer.parseInt(datosClases.get(position).getInicio().substring(0, 2)) < 12) {
                    hora = datosClases.get(position).getInicio() + " AM";
                } else {
                    hora = datosClases.get(position).getInicio() + " PM";
                }
                ((HorariosViewHolder) holder).horario.setText(hora);
                ((HorariosViewHolder) holder).mTextView.setTextColor(Color.parseColor("##5BC236"));
                ((HorariosViewHolder) holder).profesorTextView.setTextColor(Color.parseColor("##5BC236"));
                ((HorariosViewHolder) holder).horario.setTextColor(Color.parseColor("##5BC236"));
            } else {
                ((HorariosViewHolder) holder).mTextView.setText(datosClases.get(position).getClase());
                ((HorariosViewHolder) holder).profesorTextView.setText(datosClases.get(position).getInstructor());
                String hora;
                if (Integer.parseInt(datosClases.get(position).getInicio().substring(0, 2)) < 12) {
                    hora = datosClases.get(position).getInicio() + " AM";
                } else {
                    hora = datosClases.get(position).getInicio() + " PM";
                }
                ((HorariosViewHolder) holder).horario.setText(hora);
                ((HorariosViewHolder) holder).mTextView.setTextColor(Color.WHITE);
                ((HorariosViewHolder) holder).profesorTextView.setTextColor(Color.WHITE);
                ((HorariosViewHolder) holder).horario.setTextColor(Color.WHITE);

            }
        }
    }


    @Override
    public int getItemViewType(int position) {

        if(datosClases.get(position).isHeader){
            Log.d("soy header","soy header");
            return TYPE_HEADER;
        }else {
            Log.d("soy header","no soy header");
            return TYPE_ITEM;
        }


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {


        return datosClases.size();
    }


}