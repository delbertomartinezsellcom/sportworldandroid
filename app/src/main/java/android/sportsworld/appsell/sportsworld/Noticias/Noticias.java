package android.sportsworld.appsell.sportsworld.Noticias;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.sportsworld.appsell.sportsworld.Invitados.DisponiblesFragment;
import android.sportsworld.appsell.sportsworld.Modelo.Asignados;
import android.sportsworld.appsell.sportsworld.Modelo.ByPermanent;
import android.sportsworld.appsell.sportsworld.Modelo.CallBeneficios;
import android.sportsworld.appsell.sportsworld.Modelo.CallNoticias;
import android.sportsworld.appsell.sportsworld.Modelo.DataNoticias;
import android.sportsworld.appsell.sportsworld.R;
import android.sportsworld.appsell.sportsworld.conectionManager.ConnectionManager;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Noticias extends Fragment implements Adapter.OnItemClickListener {

    ViewPager viewPager;
    Adapter noticiasAdapter;
    private List<ByPermanent> permanentes;
    private DataNoticias noticiasdata;
    private RecyclerView mRecyclerView;
    // Puede ser declarado como 'RecyclerView.Adapter' o como nuetra clase adaptador 'MyAdapter'
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private int counter = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.activity_noticias, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(getActivity());

        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(getActivity());
        progressDoalog.setMax(100);
        progressDoalog.setMessage("");
        progressDoalog.setTitle("Cargando información");
// show it
        progressDoalog.show();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://app.sportsworld.com.mx")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ConnectionManager servicio =retrofit.create(ConnectionManager.class);
        Call<CallNoticias> callNoticias = servicio.getNoticias();
        callNoticias.enqueue(new Callback<CallNoticias>() {
            @Override
            public void onResponse(Call<CallNoticias> call, Response<CallNoticias> response) {
                permanentes = new ArrayList<ByPermanent>();
                noticiasdata = new DataNoticias();
                noticiasdata = response.body().getData();
                permanentes = noticiasdata.getByPermanent();
                Log.d("permanentes",Integer.toString(permanentes.size()));
                mAdapter = new Adapter(permanentes, R.layout.noticiascardview, new Adapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(ByPermanent movie, int position) {
                        //Toast.makeText(MainActivity.this, name + " - " + position, Toast.LENGTH_LONG).show();
                        Log.d("t",Integer.toString(position));
                        Intent detalleNoticia = new Intent(getContext(),DetalleNoticia.class);
                        detalleNoticia.putExtra("titulo",movie.getTitulo());
                        detalleNoticia.putExtra("html",movie.getDescripcion());
                        startActivity(detalleNoticia);
                    }
                });
                mRecyclerView.setHasFixedSize(true);
                // Añade un efecto por defecto, si le pasamos null lo desactivamos por completo
                mRecyclerView.setItemAnimator(new DefaultItemAnimator());
                mRecyclerView.setLayoutManager(mLayoutManager);
                mRecyclerView.setAdapter(mAdapter);

                progressDoalog.dismiss();
            }

            @Override
            public void onFailure(Call<CallNoticias> call, Throwable t) {
                progressDoalog.dismiss();
            }
        });

        return v;

    }


    @Override
    public void onItemClick(ByPermanent movie, int position) {


    }
}
