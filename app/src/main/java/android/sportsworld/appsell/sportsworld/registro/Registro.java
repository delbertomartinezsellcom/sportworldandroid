package android.sportsworld.appsell.sportsworld.registro;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.sportsworld.appsell.sportsworld.Mapa.Mapa;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.Club;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.Datum;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub_;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub__;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.ListClub___;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.P1;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.P2;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.P3;
import android.sportsworld.appsell.sportsworld.Modelo.Clubs.P4;
import android.sportsworld.appsell.sportsworld.Modelo.RegistroRespuesta;
import android.sportsworld.appsell.sportsworld.R;
import android.sportsworld.appsell.sportsworld.conectionManager.ConnectionManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import in.goodiebag.carouselpicker.CarouselPicker;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Registro extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private TextView  membresiaTextView,integrantesTextView,correoTextView,
            usuarioTextView,passwordTextView,confirmarPasswordTextView,codigoPostalTextView;
    private Button botonRegistrar;
    private Button botonCancelar;
    Spinner carouselPicker;
    Spinner carouselPickerClub;
    int positionPicker = 0;
    int positionPickerClub = 0;
    List<String> textItems;
    List<String> clubItems;
    List<Integer> clubIds;
    public List<Datum> datum;
    public List<ListClub__> listClubs1;
    public List<ListClub> listClub2;
    public List<ListClub_> listClub3;
    public List<ListClub___> listClub4;

    public List<P4> p4;
    public List<P3> p3;
    public List<P2> p2;
    public List<P1> p1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        carouselPicker = (Spinner) findViewById(R.id.editTipoMembresia);
        carouselPickerClub = (Spinner) findViewById(R.id.editClub);
        clubItems = new ArrayList<String>();
        clubIds = new ArrayList<Integer>();

        //Recolectar datos clubs
        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(Registro.this);
        progressDoalog.setMax(100);
        progressDoalog.setMessage("");
        progressDoalog.setTitle("Recolectando Clubs");
// show it
        progressDoalog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://app.sportsworld.com.mx")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ConnectionManager servicio =retrofit.create(ConnectionManager.class);
        Call<Club> clubs = servicio.getClubs(Double.toString(0.0),Double.toString(0.0));
        clubs.enqueue(new Callback<Club>() {
            @Override
            public void onResponse(Call<Club> call, retrofit2.Response<Club> response) {

                datum = new ArrayList<Datum>();
                datum = response.body().getData();

                p1 = new ArrayList<P1>();
                p2 = new ArrayList<P2>();
                p3 = new ArrayList<P3>();
                p4 = new ArrayList<P4>();

                p1 = datum.get(0).getP1();
                p2 = datum.get(0).getP2();
                p3 = datum.get(0).getP3();
                p4 = datum.get(0).getP4();

                listClubs1 = new ArrayList<ListClub__>();
                listClubs1 = p1.get(0).getListClub();
                List<ListClub> listClub2 = new ArrayList<ListClub>();
                listClub2 = p2.get(0).getListClub();
                List<ListClub_> listClub3 = new ArrayList<ListClub_>();
                listClub3 = p3.get(0).getListClub();
                List<ListClub_> listClub4 = new ArrayList<ListClub_>();
                listClub4 = p4.get(0).getListClub();

                Log.d("aqui", p4.get(0).getName());
                Log.d("aqui", p3.get(0).getName());
                Log.d("aqui", p2.get(0).getName());
                Log.d("aqui", p1.get(0).getName());
                Log.d("aqui", Integer.toString(listClubs1.size()));


                for (int i = 0; i < listClub3.size(); i++) {
                    clubItems.add(listClub3.get(i).getNombre());
                    try {
                        clubIds.add(listClub3.get(i).getIdun());
                    }catch (Exception ex){
                        ex.printStackTrace();
                        clubIds.add(0);
                    }

                }
                for (int i = 0; i < listClub4.size(); i++) {
                    clubItems.add(listClub4.get(i).getNombre());
                    try {
                        clubIds.add(listClub4.get(i).getIdun());
                    }catch (Exception ex){
                        ex.printStackTrace();
                        clubIds.add(0);
                    }
                }
                // CarouselPicker.CarouselViewAdapter clubItemsAdapter = new CarouselPicker.CarouselViewAdapter(getBaseContext(), clubItems, 0);
                // carouselPickerClub.setAdapter(clubItemsAdapter);

                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, clubItems);
                // Drop down layout style - list view with radio button
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                // attaching data adapter to spinner
                carouselPickerClub.setAdapter(dataAdapter);

                carouselPickerClub.setOnItemSelectedListener(Registro.this);

                carouselPickerClub.setVisibility(View.GONE);
                membresiaTextView.setVisibility(View.GONE);
                correoTextView.setVisibility(View.GONE);
                progressDoalog.dismiss();

            }
            @Override
            public void onFailure(Call<Club> call, Throwable t) {
                Log.d("aqui",t.getLocalizedMessage().toString());
                progressDoalog.dismiss();
            }
        });
        //

        membresiaTextView = (TextView) findViewById(R.id.editMembresia);
        correoTextView = (TextView) findViewById(R.id.editCorreoElectronico);
        textItems = new ArrayList<String>();
        textItems.add("Tipo de invitado");
        textItems.add("Socio");
        textItems.add("Invitado");
        textItems.add("Empleado");


        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, textItems);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        carouselPicker.setAdapter(dataAdapter);

        carouselPicker.setOnItemSelectedListener(this);


        botonRegistrar = (Button) findViewById(R.id.botonRegistrar);
        botonCancelar = (Button) findViewById(R.id.botonCancelar);
        botonCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Registro.super.onBackPressed();
            }
        });
        botonRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*if ( membresiaTextView.getText().toString().isEmpty() ||  correoTextView.toString().isEmpty() ){
                    Toast.makeText(getApplicationContext(),"Un campo esta vacio",Toast.LENGTH_LONG);
                }else{*/
                JSONObject json = new JSONObject();
                try{
                    switch (positionPicker){

                        case 1:
                            json.put("tipo", textItems.get(positionPicker).toString());
                            json.put("email", correoTextView.getText().toString() );
                            json.put("club", String.valueOf(clubIds.get(positionPicker)));
                            json.put("membresia", membresiaTextView.getText().toString());

                            break;
                        case 2:
                            json.put("tipo", textItems.get(positionPicker).toString());
                            json.put("email", correoTextView.getText().toString() );
                            json.put("id", membresiaTextView.getText().toString());
                            break;

                        case 3:
                            json.put("tipo", textItems.get(positionPicker).toString());
                            json.put("email", correoTextView.getText().toString() );

                            break;

                        default:
                            break;
                    }
                                /*json.put("email", correoTextView.getText().toString() );
                                json.put("tipo", textItems.get(positionPicker).toString());
                                json.put("club", String.valueOf(clubIds.get(positionPicker)));
                                json.put("membresia", membresiaTextView.getText().toString());
                                json.put("id", membresiaTextView.getText().toString());

                                Log.d("respuesta",textItems.get(positionPicker).toString());
                                Log.d("respuesta",clubItems.get(positionPickerClub).toString());
                                Log.d("respuesta",correoTextView.getText().toString());
                                Log.d("respuesta",membresiaTextView.getText().toString());*/
                }catch (JSONException e){
                    e.printStackTrace();
                }
                final ProgressDialog progressDoalog;
                progressDoalog = new ProgressDialog(Registro.this);
                progressDoalog.setMax(100);
                progressDoalog.setMessage("");
                progressDoalog.setTitle("Registrando");
// show it
                progressDoalog.show();
                RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"),(json.toString()));
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://cloud.sportsworld.com.mx")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                ConnectionManager servicio = retrofit.create(ConnectionManager.class);
                Call<RegistroRespuesta> usuario = servicio.setRegister(body);
                usuario.enqueue(new Callback<RegistroRespuesta>() {
                    @Override
                    public void onResponse(Call<RegistroRespuesta> call, Response<RegistroRespuesta> response) {
                        RegistroRespuesta registro = response.body();
                        if(registro == null){
                            try {
                                JSONObject jObjError = new JSONObject(response.errorBody().string());
                                show_alert(jObjError.getString("message"));
                                //Toast.makeText(getApplicationContext(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                            } catch (Exception e) {
                                //Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                show_alert(e.getMessage());
                            }
                        }else{
                            Toast.makeText(getApplicationContext(), registro.getMessage(), Toast.LENGTH_LONG).show();
                            Registro.super.onBackPressed();
                        }

                        progressDoalog.dismiss();

                    }

                    @Override
                    public void onFailure(Call<RegistroRespuesta> call, Throwable t) {
                        Toast.makeText(getApplicationContext(),t.getMessage().toString(),Toast.LENGTH_LONG).show();
                        progressDoalog.dismiss();
                    }
                });
                //}
            }
        });
    }
    AlertDialog mMyDialog; // declare AlertDialog
    public void show_alert(String msg){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(msg);
        builder.setCancelable(true);

        builder.setPositiveButton(
                "Llamar",
                new DialogInterface.OnClickListener() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:018000079727"));//change the number
                        startActivity(callIntent);
                        mMyDialog.dismiss(); // dismiss AlertDialog
                    }
                });

        mMyDialog = builder.show(); // assign AlertDialog
        return;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spinner = (Spinner) parent;
        if (spinner.getId() == R.id.editClub){
            positionPickerClub = position;
        }else{
            positionPicker = position;
            switch (positionPicker){
                case 0:
                    carouselPickerClub.setVisibility(View.GONE);
                    membresiaTextView.setVisibility(View.GONE);
                    correoTextView.setVisibility(View.GONE);
                    break;
                case 1:
                    carouselPickerClub.setVisibility(View.VISIBLE);
                    membresiaTextView.setVisibility(View.VISIBLE);
                    correoTextView.setVisibility(View.VISIBLE);
                    membresiaTextView.setHint("Membresia");
                    break;
                case 2:
                    carouselPickerClub.setVisibility(View.GONE);

                    membresiaTextView.setVisibility(View.VISIBLE);
                    correoTextView.setVisibility(View.VISIBLE);
                    membresiaTextView.setHint("id Invitado");
                    break;

                case 3:
                    carouselPickerClub.setVisibility(View.GONE);

                    membresiaTextView.setVisibility(View.GONE);
                    correoTextView.setVisibility(View.VISIBLE);

                    break;

                default:
                    break;
            }
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
