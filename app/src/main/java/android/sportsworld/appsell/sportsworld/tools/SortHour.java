package android.sportsworld.appsell.sportsworld.tools;

import android.sportsworld.appsell.sportsworld.Modelo.Clases.DatumClases;

import java.util.Comparator;

public class SortHour implements Comparator<DatumClases>{

    @Override
    public int compare(DatumClases o1, DatumClases o2) {
        String o1Temp = o1.getInicio().substring(0, 2);
        String o2Temp = o2.getInicio().substring(0, 2);

        if (o1.getInicio().charAt(0) == '0'){
            o1Temp = Character.toString(o1.getInicio().charAt(1));
        }
        if (o2.getInicio().charAt(0)=='0'){
            o2Temp = Character.toString(o2.getInicio().charAt(1));
        }

        return Integer.parseInt(o1Temp)-Integer.parseInt(o2Temp);
    }
}
