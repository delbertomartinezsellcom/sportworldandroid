package android.sportsworld.appsell.sportsworld.Modelo;

public class CancelarClase {

    public String message;
    public String message2;
    public Boolean status;

    public CancelarClase(String message, String message2, Boolean status) {
        this.message = message;
        this.message2 = message2;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage2() {
        return message2;
    }

    public void setMessage2(String message2) {
        this.message2 = message2;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
