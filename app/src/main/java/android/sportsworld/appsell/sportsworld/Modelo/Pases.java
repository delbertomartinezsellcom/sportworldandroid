package android.sportsworld.appsell.sportsworld.Modelo;

public class Pases {
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFinVigencia() {
        return finVigencia;
    }

    public void setFinVigencia(String finVigencia) {
        this.finVigencia = finVigencia;
    }

    String nombre;

    public Pases(String nombre, String finVigencia) {
        this.nombre = nombre;
        this.finVigencia = finVigencia;
    }

    String finVigencia;
}
