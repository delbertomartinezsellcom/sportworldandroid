package android.sportsworld.appsell.sportsworld.Modelo.Clubs;

import java.util.List;

public class Club {
    public Boolean status;
    public String message;
    public List<Datum>data;

    public Club(Boolean status, String message, List<Datum> data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
}
