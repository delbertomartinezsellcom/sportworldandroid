package android.sportsworld.appsell.sportsworld.tools.calendario.adapter;

import android.graphics.drawable.Drawable;
import android.sportsworld.appsell.sportsworld.R;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;



/**
 * @author Mulham-Raee
 * @since v1.0.0
 */
class DateViewHolder extends RecyclerView.ViewHolder {

    TextView textTop;
    TextView textMiddle;
    TextView textBottom;
    Drawable backgroundStrokeCircle;
    View selectionView;
    View layoutContent;
    RecyclerView eventsRecyclerView;

    DateViewHolder(View rootView) {
        super(rootView);
        textTop = rootView.findViewById(R.id.hc_text_top);
        textMiddle = rootView.findViewById(R.id.hc_text_middle);
        backgroundStrokeCircle = textMiddle.getBackground();
        textBottom = rootView.findViewById(R.id.hc_text_bottom);
        layoutContent = rootView.findViewById(R.id.hc_layoutContent);
        selectionView = rootView.findViewById(R.id.hc_selector);
        eventsRecyclerView = rootView.findViewById(R.id.hc_events_recyclerView);
    }
}
